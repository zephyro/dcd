<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Новости</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Новости</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <!-- Nav heading -->
                    <nav class="heading-nav">
                        <div class="container">
                            <ul>
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">Наши клиенты</a></li>
                                <li><a href="#">Новости</a></li>
                                <li class="active"><a href="#">Галерея</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- -->
                    <div class="category">
                        <div class="container">
                            <ul>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_01.jpg" class="img-responsive" alt="">
                                        <div class="category-text">Мебель для руководителя</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_06.jpg" class="img-responsive" alt="">
                                        <div class="category-text">мини-кухни и шкафы</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_03.jpg" class="img-responsive" alt="">
                                        <div class="category-text">Ресепшн</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_04.jpg" class="img-responsive" alt="">
                                        <div class="category-text">офисные кресла и стулья</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_05.jpg" class="img-responsive" alt="">
                                        <div class="category-text">Мебель для компьютера</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_06.jpg" class="img-responsive" alt="">
                                        <div class="category-text">мини-кухни и шкафы</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_07.jpg" class="img-responsive" alt="">
                                        <div class="category-text">мягкая офисная мебель</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_08.jpg" class="img-responsive" alt="">
                                        <div class="category-text">Переговорная зона</div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- script -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
