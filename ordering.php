<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">

            <div class="page-inner">
                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Оформление заказа</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Оформление заказа</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <!-- Nav heading -->
                    <nav class="heading-nav">
                        <div class="container">
                            <ul class="tabs-nav">
                                <li class="active"><a href="#" data-target=".tab01">Для физических лиц</a></li>
                                <li><a href="#" data-target=".tab02">Для юридических лиц</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- -->

                    <div class="content content-overflow">
                        <div class="container">
                            <div class="border-wrap">

                                <div class="tabs">

                                    <div class="tabs-item tab01 active">

                                        <div class="heading-second"><h3>Личные данные</h3></div>
                                        <div class="content-wrap">
                                            <div class="form-wrap">

                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">ФИО<sup>*</sup></label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control" name="" placeholder="Иванов Иван Иванович">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Телефон<sup>*</sup></label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-m" name="" placeholder="+7-___-__-__">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Email</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-m" name="" placeholder="ivanov@website.ru">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Адрес доставки<sup>*</sup><br/><small>Доставка только по москве</small></label>
                                                    </li>
                                                    <li>
                                                        <textarea class="form-control" placeholder="Улица, дом, строение, корпус, квартира" rows="2"></textarea>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="heading-second"><h3>Платежная система</h3></div>
                                        <div class="content-wrap">
                                            <p>
                                                <label class="radio">
                                                    <input class="radio-input" type="radio" name="system" checked>
                                                    <span class="radio-text">
                                                        <strong>Наличные курьеру</strong>
                                                        <br/>
                                                        <span class="radio-info">Оплата наличными осуществляется до получения товара. Мы пришлём курьера с оформленными документами и чеком. Стоимость услуги 100 рублей + цена проезда в оба конца.</span>
                                                    </span>
                                                </label>
                                            </p>
                                            <p>
                                                <label class="radio">
                                                    <input class="radio-input" type="radio" name="system">
                                                    <span class="radio-text">
                                                        <strong>Сбербанк</strong>
                                                        <br/><span class="radio-info">Оплата наличными осуществляется до получения товара. Мы пришлём курьера с оформленными документами и чеком. Стоимость услуги 100 рублей + цена проезда в оба конца.</span>

                                                    </span>
                                                </label>
                                            </p>
                                        </div>

                                        <div class="heading-second"><h3>Состав заказа</h3></div>
                                        <div class="order-table">
                                            <div class="table-header">
                                                <ul>
                                                    <li>Наименование</li>
                                                    <li>Сборка</li>
                                                    <li>цена</li>
                                                    <li>кол-во</li>
                                                    <li>Сумма</li>
                                                </ul>
                                            </div>
                                            <div class="table-body">
                                                <ul>
                                                    <li>
                                                        <b>Наименование</b>
                                                        <span>Конференц-приставка К-967</span>
                                                    </li>
                                                    <li>
                                                        <b>Сборка</b>
                                                        <span>Есть</span>
                                                    </li>
                                                    <li>
                                                        <b>цена</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                    <li>
                                                        <b>кол-во</b>
                                                        <span>1</span>
                                                    </li>
                                                    <li>
                                                        <b>Сумма</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <b>Наименование</b>
                                                        <span>Конференц-приставка К-967</span>
                                                    </li>
                                                    <li>
                                                        <b>Сборка</b>
                                                        <span>Есть</span>
                                                    </li>
                                                    <li>
                                                        <b>цена</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                    <li>
                                                        <b>кол-во</b>
                                                        <span>1</span>
                                                    </li>
                                                    <li>
                                                        <b>Сумма</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <b>Наименование</b>
                                                        <span>Конференц-приставка К-967</span>
                                                    </li>
                                                    <li>
                                                        <b>Сборка</b>
                                                        <span>Есть</span>
                                                    </li>
                                                    <li>
                                                        <b>цена</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                    <li>
                                                        <b>кол-во</b>
                                                        <span>1</span>
                                                    </li>
                                                    <li>
                                                        <b>Сумма</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <b>Наименование</b>
                                                        <span>Конференц-приставка К-967</span>
                                                    </li>
                                                    <li>
                                                        <b>Сборка</b>
                                                        <span>Есть</span>
                                                    </li>
                                                    <li>
                                                        <b>цена</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                    <li>
                                                        <b>кол-во</b>
                                                        <span>1</span>
                                                    </li>
                                                    <li>
                                                        <b>Сумма</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="table-footer">
                                                <ul>
                                                    <li>Цена:</li>
                                                    <li>16 614 <i class="fa fa-ruble"></i></li>
                                                </ul>
                                                <ul>
                                                    <li>Скидка по промокоду</li>
                                                    <li>- 1 200 <i class="fa fa-ruble"></i></li>
                                                </ul>
                                                <ul>
                                                    <li><span class="color-orange">Скидка 5%:</span></li>
                                                    <li>- 830 <i class="fa fa-ruble"></i></li>
                                                </ul>
                                                <ul>
                                                    <li>Итого:</li>
                                                    <li><span class="order-total">14 584 <i class="fa fa-ruble"></i></span></li>
                                                </ul>

                                            </div>
                                            <div class="order-checkout">
                                                <button type="submit" class="btn btn-orange btn-md">Оформить</button>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="tabs-item tab02">
                                        <div class="heading-second"><h3>Информация для оплаты и доставки заказа</h3></div>
                                        <div class="content-wrap">

                                            <h4>Данные компании</h4>
                                            <div class="form-wrap">

                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Название компании<sup>*</sup></label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Юридический адрес<sup>*</sup></label>
                                                    </li>
                                                    <li>
                                                        <textarea class="form-control" placeholder="Индекс, город, улица, дом, строение, корпус,квартира" rows="2"></textarea>
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">ИНН<sup>*</sup></label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-m" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">КПП<sup>*</sup></label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-m" name="" placeholder="">
                                                    </li>
                                                </ul>
                                            </div>
                                            <br/>
                                            <h4>Контактная информация</h4>
                                            <div class="form-wrap">

                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Контактное лицо</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control" name="" placeholder="Иванов Иван Иванович">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Телефон<sup>*</sup></label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-m" name="" placeholder="+7-___-__-__">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Email</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-m" name="" placeholder="ivanov@website.ru">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Адрес доставки<sup>*</sup><br/><small>Доставка только по москве</small></label>
                                                    </li>
                                                    <li>
                                                        <textarea class="form-control" placeholder="Улица, дом, строение, корпус, квартира" rows="2"></textarea>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="heading-second"><h3>Платежная система</h3></div>
                                        <div class="content-wrap">
                                            <p>
                                                <label class="radio">
                                                    <input class="radio-input" type="radio" name="system" checked>
                                                    <span class="radio-text">
                                                        <strong>Наличные курьеру</strong>
                                                        <br/>
                                                        <span class="radio-info">Оплата наличными осуществляется до получения товара. Мы пришлём курьера с оформленными документами и чеком. Стоимость услуги 100 рублей + цена проезда в оба конца.</span>
                                                    </span>
                                                </label>
                                            </p>
                                            <p>
                                                <label class="radio">
                                                    <input class="radio-input" type="radio" name="system">
                                                    <span class="radio-text">
                                                        <strong>Счет</strong>
                                                    </span>
                                                </label>
                                            </p>
                                        </div>

                                        <div class="heading-second"><h3>Состав заказа</h3></div>
                                        <div class="order-table">
                                            <div class="table-header">
                                                <ul>
                                                    <li>Наименование</li>
                                                    <li>Сборка</li>
                                                    <li>цена</li>
                                                    <li>кол-во</li>
                                                    <li>Сумма</li>
                                                </ul>
                                            </div>
                                            <div class="table-body">
                                                <ul>
                                                    <li>
                                                        <b>Наименование</b>
                                                        <span>Конференц-приставка К-967</span>
                                                    </li>
                                                    <li>
                                                        <b>Сборка</b>
                                                        <span>Есть</span>
                                                    </li>
                                                    <li>
                                                        <b>цена</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                    <li>
                                                        <b>кол-во</b>
                                                        <span>1</span>
                                                    </li>
                                                    <li>
                                                        <b>Сумма</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <b>Наименование</b>
                                                        <span>Конференц-приставка К-967</span>
                                                    </li>
                                                    <li>
                                                        <b>Сборка</b>
                                                        <span>Есть</span>
                                                    </li>
                                                    <li>
                                                        <b>цена</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                    <li>
                                                        <b>кол-во</b>
                                                        <span>1</span>
                                                    </li>
                                                    <li>
                                                        <b>Сумма</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <b>Наименование</b>
                                                        <span>Конференц-приставка К-967</span>
                                                    </li>
                                                    <li>
                                                        <b>Сборка</b>
                                                        <span>Есть</span>
                                                    </li>
                                                    <li>
                                                        <b>цена</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                    <li>
                                                        <b>кол-во</b>
                                                        <span>1</span>
                                                    </li>
                                                    <li>
                                                        <b>Сумма</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>
                                                <ul>
                                                    <li>
                                                        <b>Наименование</b>
                                                        <span>Конференц-приставка К-967</span>
                                                    </li>
                                                    <li>
                                                        <b>Сборка</b>
                                                        <span>Есть</span>
                                                    </li>
                                                    <li>
                                                        <b>цена</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                    <li>
                                                        <b>кол-во</b>
                                                        <span>1</span>
                                                    </li>
                                                    <li>
                                                        <b>Сумма</b>
                                                        <span>5 298 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="table-footer">
                                                <ul>
                                                    <li>Цена:</li>
                                                    <li>16 614 <i class="fa fa-ruble"></i></li>
                                                </ul>
                                                <ul>
                                                    <li>Скидка по промокоду</li>
                                                    <li>- 1 200 <i class="fa fa-ruble"></i></li>
                                                </ul>
                                                <ul>
                                                    <li><span class="color-orange">Скидка 5%:</span></li>
                                                    <li>- 830 <i class="fa fa-ruble"></i></li>
                                                </ul>
                                                <ul>
                                                    <li>Итого:</li>
                                                    <li><span class="order-total">14 584 <i class="fa fa-ruble"></i></span></li>
                                                </ul>

                                            </div>
                                            <div class="order-checkout">
                                                <button type="submit" class="btn btn-orange btn-md">Оформить</button>
                                            </div>
                                        </div>

                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>


                </section>
                <!-- -->
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>


