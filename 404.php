<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>
        <div class="page">
            <div class="page-inner">
                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Heading -->
                                <h1>Ошибка 404</h1>
                                <!-- -->

                                <div class="error-block"></div>

                            </div>
                        </div>
                    </div>
                    <!-- -->


                    <!-- Main content -->
                    <div class="content">
                        <div class="container">
                            <div class="border-wrap">
                                <ul class="error">
                                    <li>
                                        <div class="error-text">
                                            <p>Данная страница не существует или удалена администратором. Попробуйте начать поиск с начала или воспользуйтесь <a href="#">подборщиком мебели.</p>
                                            <a href="#" class="btn btn-orange">На главную</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="error-image">
                                            <img src="img/404_image.jpg" class="img-responsive" alt="">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- -->

                </section>
                <!-- -->
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>




        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
