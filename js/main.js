
// Header Menu

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('open');
        pull.toggleClass('open');
        $('.navbar-bg').toggleClass('open');
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 1270 && menu.is(':hidden')) {
            menu.removeClass('open');
            pull.removeClass('open');
            $('.navbar-bg').removeClass('open');
        }
    });

    $('.navbar').click(function (event) {
        var w = $(window).width();
        if((w < 1270) && ($(event.target).closest(".navbar-inner").length === 0) && ($(event.target).closest(".navbar-toggle").length === 0)) {
            $(".navbar-toggle").removeClass('open');
            $(".navbar").removeClass('open');
            $('.navbar-bg').removeClass('open');
        }
    });


    $('.navbar-close').click(function (event) {
        var w = $(window).width();
        menu.removeClass('open');
        pull.removeClass('open');
        $('.navbar-bg').removeClass('open');
    });

    $('.topnav > .dropdown').on('mouseover',function () {
        $('.topnav').addClass('open');
    });

    $('.topnav > .dropdown').on('mouseout',function () {
        $('.topnav').removeClass('open');
    });

});

$(function($){
    var point = $('.page-inner');
    var label = $('.navbar-label');
    $h = label.offset().top;

    $(window).scroll(function(){

        if ( $(window).scrollTop() > $h) {
            point.addClass('fix-top');
        }else{
            //Иначе возвращаем всё назад. Тут вы вносите свои данные
            point.removeClass('fix-top');
        }
    });
});




$('.header-nav-toggle').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.fix-top').toggleClass('open-nav');
});


$('.navbar-delete').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.fix-top').toggleClass('open-nav');
});





$('.jumbotron-image-slider').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.jumbotron-slider'
});

$('.jumbotron-slider').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fa fa-angle-right"></i></span>',
    asNavFor: '.jumbotron-image-slider',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                arrows: false
            }
        }
    ]
});

$('.goods-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fa fa-angle-right"></i></span>',
    responsive: [
        {
            breakpoint: 1270,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }
    ]

});

$('.often-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fa fa-angle-right"></i></span>',
    responsive: [
        {
            breakpoint: 1270,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }
    ]

});


$('.leaders-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 600,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fa fa-angle-right"></i></span>',
    responsive: [
        {
            breakpoint: 1270,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }
    ]

});


$('.vendors-slider').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 600,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fa fa-angle-right"></i></span>',
    responsive: [
        {
            breakpoint: 1270,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }
    ]

});


$('.contact-gallery').slick({
    dots: true,
    arrows: false,
    infinite: false,
    speed: 600,
    autoplay: true,
    autoplaySpeed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1
});


// Accordion

$('.accordion-title a').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.accordion-item').toggleClass('open');
    $(this).closest('.accordion-item').find('.accordion-content').slideToggle(300);
});


$('.accordion-toggle').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.accordion-item').removeClass('open');
    $(this).closest('.accordion-content').slideToggle(300);
});


// Tabs


$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $('.tabs');

    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


$('.vacancy-title').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.vacancy').find('li').removeClass('open');
    $(this).closest('li').toggleClass('open');
});


$('.header-basket-toggle').on('click touchstart', function(e){
    e.preventDefault();
    $(this).closest('.header-card-box').addClass('open');
    $(this).closest('.header').addClass('open-basket');
});

$('.mini-cart-close').on('click touchstart', function(e){
    e.preventDefault();
    $('.header-card-box').removeClass('open');
    $(this).closest('.header').removeClass('open-basket');
});

$('.mini-cart-bg').click(function (event) {
    if(($(event.target).closest(".mini-cart").length === 0)) {
        $(".header-card-box").removeClass('open');
        $(this).closest('.header').removeClass('open-basket');
    }
});


// Input Number

$(function() {
    $('.minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
});


jQuery(document).ready(function(){
    jQuery('.content-scroll').scrollbar();
});

jQuery(document).ready(function(){
    jQuery('.filter-scroll').scrollbar();
});

jQuery(document).ready(function(){
    jQuery('.mini-card-scroll').scrollbar();
});


$(".btn-modal-color").fancybox({
    'padding'    : 0,
    baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
    '<div class="fancybox-bg"></div>' +
    '<div class="fancybox-slider-wrap">' +
    '<div class="fancybox-slider fancybox-slider-padding"></div>' +
    '</div>' +
    '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
    '</div>',
    closeTpl : '<button data-fancybox-close class="modal-close"></button>'
});



$('.filter-toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $('.cat-filter').toggleClass('open');
});



// Меню сортировки

$(function() {

    $('.select-nav-current').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.select-nav').toggleClass('open');
    });

    $('.select-nav-bg').click(function (event) {
        if($(event.target).closest(".select-nav-wrap").length === 0) {
            $(this).closest('.select-nav').removeClass('open');
        }
    });

    $('.select-nav-dropdown input[type="radio"]').change(function(e) {
        var box = $(this).closest('.select-nav');
        box.removeClass('open');
        var txt = $(this).val();
        console.log(txt);
        box.find('.select-nav-current span').text(txt);
    });

});


$(function() {

    $('.filter-nav-current').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.filter-nav').toggleClass('open');
    });

    $('.filter-nav-bg').click(function (event) {
        if($(event.target).closest(".filter-nav-wrap").length === 0) {
            $(this).closest('.filter-nav').removeClass('open');
        }
    });

    $('.filter-nav-dropdown input[type="radio"]').change(function(e) {
        var box = $(this).closest('.filter-nav');
        box.removeClass('open');
    });
});



$('.image-large').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.image-thumb-slider'
});

$('.image-thumb-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.image-large',
    dots: false,
    arrows: true,
    vertical: true,
    focusOnSelect: true,
    prevArrow: '<span class="image-nav prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="image-nav next"><i class="fa fa-angle-right"></i></span>',
    responsive: [
        {
            breakpoint: 1270,
            settings: {
                vertical: false
            }
        },
        {
            breakpoint: 992,
            settings: {

            }
        },
        {
            breakpoint: 768,
            settings: {
                vertical: false
            }
        }
    ]
});

$('.series-tabs-nav a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $('.series-tabs');

    $(this).closest('.series-tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.series-tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});

$('.tabs-header').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $('.series-tabs');

    $(this).closest('.series-tabs').find('.series-tabs-item').removeClass('active');
    $(this).closest('.series-tabs-item').addClass('active');

    box.find('.series-tabs-nav li').removeClass('active');
    box.find(tab).addClass('active');
});


// Filter




$('.filter-btn-inside').on('click touchstart', function(e){
    e.preventDefault();
    $('.rows').addClass('open');
    $('.filter-btn-inside').addClass('open');

});

$('.sidebar-filter-close').on('click touchstart', function(e){
    e.preventDefault();
    $('.rows').removeClass('open');
    $('.filter-btn-inside').removeClass('open');

});




$('.filter-btn-toggle').on('click touchstart', function(e){
    e.preventDefault();
    $('.main-filter').addClass('open');

});

$('.filter-close').on('click touchstart', function(e){
    e.preventDefault();
    $('.main-filter').removeClass('open');

});


$('.filter-group input[type="radio"]').on("change", function () {
    var box = $(this).closest('.filter-group');
    box.find('.filter-group-content').slideToggle();
    box.toggleClass('open');
});


$('.filter-group input[type="checkbox"]').on("change", function () {
    var box = $(this).closest('.filter-group');
});

$('.filter-group-header').on('click touchstart', function(e){
    e.preventDefault();
    var box = $(this).closest('.filter-group');
    box.toggleClass('open');
    box.find('.filter-group-content').slideToggle();
});


$('.filter-group-close').on('click touchstart', function(e){
    e.preventDefault();
    var box = $(this).closest('.filter-group');
    box.find('.filter-group-content').slideToggle();
    box.toggleClass('open');
});




var $amountOne = $("#range-price");

$amountOne.ionRangeSlider({
    type: "double",
    min: 3587,
    max: 45000,
    grid: false,
    hide_min_max: true
});

$amountOne.on("change", function () {
    var $this       = $(this),
        from = $this.data("from"),
        to = $this.data("to");

    $('.range-price-from').text(from);
    $('.range-price-to').text(to);
});


var $amountTwo = $("#range-thick");

$amountTwo.ionRangeSlider({
    type: "single",
    min: 0,
    max: 30,
    from: 5,
    to: 30,
    grid: false,
    postfix: "mm"
});
