<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Оплата</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Оплата</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <!-- Nav heading -->
                    <nav class="heading-nav">
                        <div class="container">
                            <ul>
                                <li><a href="#">Скидки и акции</a></li>
                                <li class="active"><a href="#">Получить скидку</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- -->


                    <div class="content content-overflow">
                        <div class="container">
                            <div class="heading-gray">
                                <div class="border-wrap">
                                    <h4>Вы наш гость</h4>
                                </div>
                            </div>
                            <div class="border-wrap">
                                <div class="text">Мы очень рады видеть Вас на своем сайте и дарим ПОДАРОК к первому заказу! Введите слово «подарок» в поле промо-кода при оформлении корзины или: Просто оформите корзину, и мы привезем Ваш заказ вместе с комплиментом от нашего магазина.</div>
                            </div>
                            <div class="heading-gray">
                                <div class="border-wrap">
                                    <h4>Станьте нашим другом</h4>
                                </div>
                            </div>
                            <div class="border-wrap">
                                <div class="text">Подпишитесь на наши новости и станьте Другом ДСД! Специально для наших друзей действует каскад бонусов:</div>
                            </div>

                            <table class="table-striped">
                                <tr>
                                    <th>Предложение</th>
                                    <th>скидка</th>
                                </tr>
                                <tr>
                                    <td>На любой заказ с сайта постоянная скидка</td>
                                    <td>2%</td>
                                </tr>
                                <tr>
                                    <td>За каждые 10 000 рублей прибавляем скидку</td>
                                    <td>+1%</td>
                                </tr>
                                <tr>
                                    <td>Если сумма заказа свыше 50 000 рублей</td>
                                    <td>Привезем все бесплатно</td>
                                </tr>
                                <tr>
                                    <td>Для заказа суммой свыше 100 000 рублей</td>
                                    <td>Доставка, подъем и сборка - бесплатно</td>
                                </tr>
                            </table>
                            <div class="border-wrap">
                                <div class="text">А также Вы всегда будете знать о проходящих акциях «только для своих», о наших новинках и самых лучших предложениях!</div>
                            </div>
                            <div class="heading-gray">
                                <div class="border-wrap">
                                    <h4>Интересная скидка</h4>
                                </div>
                            </div>
                            <div class="heading-dark">
                                <div class="border-wrap">
                                    Настоящие друзья хорошо друг друга знают! На страницах нашего сайта мы спрятали пять кодовых слов. Найдите все слова и получите дополнительную скидку 5% или по проценту за каждое найденное слово, за проявленный интерес!
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- script -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
