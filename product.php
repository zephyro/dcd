<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li><a href="#">Мебель для руководителей</a></li>
                                    <li><a href="#">Приоритет</a></li>
                                    <li>Стол рабочий К-961</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Стол рабочий К-961</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <!-- Main content -->
                    <div class="content content-overflow">

                        <div class="container">
                            <div class="border-wrap">
                                <div class="product">

                                    <div class="product-image">
                                        <a href="images/table_img.png" data-fancybox="product">
                                            <img src="images/table_img.png" class="img-responsive" alt="">
                                        </a>
                                    </div>

                                    <div class="product-purchase">

                                        <div class="colors">
                                            <ul class="colors-list">
                                                <li>
                                                    <label>
                                                        <input type="radio" name="radio">
                                                        <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="radio">
                                                        <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="radio">
                                                        <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="radio">
                                                        <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="radio">
                                                        <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                    </label>
                                                </li>
                                                <li class="last">
                                                    <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                </li>
                                            </ul>
                                        </div>

                                        <ul class="product-assembly">
                                            <li>
                                                <label>
                                                    <input type="checkbox" class="input-assembly" name="assembly" checked >
                                                    <div class="input-assembly-text">
                                                        <svg class="ico-svg" viewBox="0 0 16 16">
                                                            <use xlink:href="img/sprite-icons.svg#icon-assembly"></use>
                                                        </svg>
                                                        <span>Сборка</span>
                                                    </div>
                                                </label>
                                            </li>
                                            <li>1 800 <i class="fa fa-ruble"></i></li>
                                        </ul>

                                        <ul class="product-buy">
                                            <li>
                                                <div class="form-number">
                                                    <span class="minus">-</span>
                                                    <span class="plus">+</span>
                                                    <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                </div>
                                            </li>
                                            <li>
                                                <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                            </li>
                                        </ul>
                                        <ul class="product-action">
                                            <li><button type="submit" class="btn btn-orange">В корзину</button></li>
                                            <li><a href="#" class="btn btn">Быстрая покупка</a></li>
                                        </ul>

                                    </div>

                                    <div class="product-info">
                                        <p><strong>Характеристики:</strong></p>
                                        <div class="product-table">
                                            <ul>
                                                <li><span>Артикул:</span></li>
                                                <li><span>К-961</span></li>
                                            </ul>
                                            <ul>
                                                <li><span>Ширина:</span></li>
                                                <li><span>1800 мм</span></li>
                                            </ul>
                                            <ul>
                                                <li><span>Глубина:</span></li>
                                                <li><span>900 мм</span></li>
                                            </ul>
                                            <ul>
                                                <li><span>Высота:</span></li>
                                                <li><span>750 мм</span></li>
                                            </ul>
                                            <ul>
                                                <li><span>Вес упаковки:</span></li>
                                                <li><span>120 кг</span></li>
                                            </ul>
                                            <ul>
                                                <li><span>Производитель:</span></li>
                                                <li><span>0.203 куб. м.</span></li>
                                            </ul>
                                            <ul>
                                                <li><span>Производитель:</span></li>
                                                <li><span>Россия</span></li>
                                            </ul>
                                        </div>
                                        <p><strong>Описание:</strong> Стол рабочий К-961</p>
                                    </div>

                                </div>

                                <div class="often">
                                    <div class="often-heading">
                                        <div class="often-heading-text">С этим товаром часто покупают</div>
                                    </div>
                                    <div class="often-slider">
                                        <div class="often-item">
                                            <div class="showcase">
                                                <form class="form">

                                                    <!-- Favorite -->
                                                    <?php include('inc/favorite.inc.php') ?>
                                                    <!-- -->

                                                    <a href="#" class="showcase-image">
                                                        <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="showcase-content">
                                                        <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                        <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                        <div class="colors colors-selected">
                                                            <ul class="colors-list">
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                                </li>
                                                            </ul>

                                                            <ul class="colors-current">
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio" checked disabled>
                                                                        <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                                </li>
                                                                <li>
                                                                    <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <ul class="showcase-purchase">
                                                            <li>
                                                                <div class="form-number">
                                                                    <span class="minus">-</span>
                                                                    <span class="plus">+</span>
                                                                    <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                                <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>

                                                        <button type="submit" class="btn">В корзину</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="often-item">
                                            <div class="showcase">
                                                <form class="form">

                                                    <!-- Favorite -->
                                                    <?php include('inc/favorite.inc.php') ?>
                                                    <!-- -->

                                                    <a href="#" class="showcase-image">
                                                        <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="showcase-content">
                                                        <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                        <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                        <div class="colors">
                                                            <ul class="colors-list">
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <ul class="showcase-purchase">
                                                            <li>
                                                                <div class="form-number">
                                                                    <span class="minus">-</span>
                                                                    <span class="plus">+</span>
                                                                    <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                                <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>

                                                        <button type="submit" class="btn">В корзину</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="often-item">
                                            <div class="showcase">
                                                <form class="form">

                                                    <!-- Favorite -->
                                                    <?php include('inc/favorite.inc.php') ?>
                                                    <!-- -->

                                                    <a href="#" class="showcase-image">
                                                        <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="showcase-content">
                                                        <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                        <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                        <div class="colors">
                                                            <ul class="colors-list">
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_06.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_07.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <ul class="showcase-purchase">
                                                            <li>
                                                                <div class="form-number">
                                                                    <span class="minus">-</span>
                                                                    <span class="plus">+</span>
                                                                    <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>

                                                        <button type="submit" class="btn">В корзину</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="often-item">
                                            <div class="showcase">
                                                <form class="form">

                                                    <!-- Favorite -->
                                                    <?php include('inc/favorite.inc.php') ?>
                                                    <!-- -->

                                                    <a href="#" class="showcase-image">
                                                        <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="showcase-content">
                                                        <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                        <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                        <div class="colors colors-selected">
                                                            <ul class="colors-list">
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio">
                                                                        <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li class="last">
                                                                    <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                                </li>
                                                            </ul>

                                                            <ul class="colors-current">
                                                                <li>
                                                                    <label>
                                                                        <input type="radio" name="radio" checked disabled>
                                                                        <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                                </li>
                                                                <li>
                                                                    <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <ul class="showcase-purchase">
                                                            <li>
                                                                <div class="form-number">
                                                                    <span class="minus">-</span>
                                                                    <span class="plus">+</span>
                                                                    <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                                <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>

                                                        <button type="submit" class="btn">В корзину</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </section>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->
        </div>

        <!-- script -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
