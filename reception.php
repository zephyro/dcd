<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Ресепшн</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Ресепшн</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <!-- Main content -->
                    <div class="content">
                        <div class="container">
                            <div class="border-wrap">

                                <ul class="gallery">
                                    <li>
                                        <a href="#"  class="gallery-image">
                                            <img src="images/reception_01.jpg" class="img-responsive" alt="">
                                        </a>
                                        <span>от 120 000 рублей п.м. (2008 г)</span>
                                    </li>
                                    <li>
                                        <a href="#"  class="gallery-image">
                                            <img src="images/reception_02.jpg" class="img-responsive" alt="">
                                        </a>
                                        <span>от 75 000 рублей п.м. (2009 г)</span>
                                    </li>
                                    <li>
                                        <a href="#"  class="gallery-image">
                                            <img src="images/reception_03.jpg" class="img-responsive" alt="">
                                        </a>
                                        <span>от 94 000 рублей п.м. (2010 г)</span>
                                    </li>
                                    <li>
                                        <a href="#"  class="gallery-image">
                                            <img src="images/reception_04.jpg" class="img-responsive" alt="">
                                        </a>
                                        <span>от 94 000 рублей п.м. (2010 г)</span>
                                    </li>
                                    <li>
                                        <a href="#"  class="gallery-image">
                                            <img src="images/reception_01.jpg" class="img-responsive" alt="">
                                        </a>
                                        <span>от 120 000 рублей п.м. (2008 г)</span>
                                    </li>
                                    <li>
                                        <a href="#"  class="gallery-image">
                                            <img src="images/reception_02.jpg" class="img-responsive" alt="">
                                        </a>
                                        <span>от 75 000 рублей п.м. (2009 г)</span>
                                    </li>
                                    <li>
                                        <a href="#"  class="gallery-image">
                                            <img src="images/reception_03.jpg" class="img-responsive" alt="">
                                        </a>
                                        <span>от 94 000 рублей п.м. (2010 г)</span>
                                    </li>
                                    <li>
                                        <a href="#"  class="gallery-image">
                                            <img src="images/reception_04.jpg" class="img-responsive" alt="">
                                        </a>
                                        <span>от 94 000 рублей п.м. (2010 г)</span>
                                    </li>
                                </ul>

                                <div class="content-box">
                                    <p>Ресепшн на заказ изготавливаем по вашим размерам. От экономичных (от 5 000 рублей за погонный метр) до премиум класса (более 100 000 рублей). Срок изготовления зависит от размера, сложности и используемых материалов, как правило от 2-х до 8-ми недель.</p>
                                    <p>Порядок работы:</p>
                                    <ol>
                                        <li>формирование идеи, определение концепции и цветового решения;</li>
                                        <li>набросок эскиза для понимания конфигурации, функциональных возможностей изделия и примерный расчёт стоимости;</li>
                                        <li>изготовление чертежей на базе выбранных материалов в 2D и 3D проекциях, окончательный расчёт стоимости изделия;</li>
                                        <li>после утверждения Заказчиком сметы и предоплаты запуск в производсвто;</li>
                                        <li>доставка, сборка и установка изделия.</li>
                                    </ol>
                                    <p>Чем раньше будет принято решение об изготовлении заказного изделия, тем короче сроки до его инсталяции в офисе Заказчика!</p>

                                    <p>Звоните (495) 661-71-58 или приезжайте (<a href="#">адрес</a>). Рады будем сотрудничеству.</p>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- -->

                </section>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- script -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
