<section class="leaders-block">
    <div class="container">
        <div class="inner">
            <div class="leaders-heading"><span>Лидеры продаж</span></div>
            <div class="leaders-slider">
                <div class="leaders-slider-item">
                    <a href="#" class="leaders">
                        <img src="images/leaders_01.jpg" class="img-responsive" alt="">
                        <div class="leaders-text">кабинеты руководителей</div>
                        <span class="leaders-tags">Vasanta</span>
                    </a>
                </div>
                <div class="leaders-slider-item">
                    <a href="#" class="leaders">
                        <img src="images/leaders_02.jpg" class="img-responsive" alt="">
                        <div class="leaders-text">Мебель для персонала</div>
                        <span class="leaders-tags">Метта</span>
                    </a>
                </div>
                <div class="leaders-slider-item">
                    <a href="#" class="leaders">
                        <img src="images/leaders_03.jpg" class="img-responsive" alt="">
                        <div class="leaders-text">кресла для руководителей</div>
                        <span class="leaders-tags">Новый стиль</span>
                    </a>
                </div>
                <div class="leaders-slider-item">
                    <a href="#" class="leaders">
                        <img src="images/leaders_01.jpg" class="img-responsive" alt="">
                        <div class="leaders-text">кресла и стулья</div>
                        <span class="leaders-tags">Vasanta</span>
                    </a>
                </div>
                <div class="leaders-slider-item">
                    <a href="#" class="leaders">
                        <img src="images/leaders_02.jpg" class="img-responsive" alt="">
                        <div class="leaders-text">кабинеты руководителей</div>
                        <span class="leaders-tags">Метта</span>
                    </a>
                </div>
                <div class="leaders-slider-item">
                    <a href="#" class="leaders">
                        <img src="images/leaders_01.jpg" class="img-responsive" alt="">
                        <div class="leaders-text">Мебель для персонала</div>
                        <span class="leaders-tags">Новый стиль</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>