<header class="header">

    <div class="container">


        <ul class="header-tile">

            <li>
                <a href="#" class="header-nav navbar-toggle" data-target=".navbar">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>

                <a href="#" class="header-nav-toggle" data-target=".navbar">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>

                <div class="navbar-bg"></div>
            </li>

            <li>
                <a href="#" class="header-logo">
                    <img src="img/header_logo.png" class="img-responsive" alt="">
                </a>

            </li>

            <li>
                <ul class="header-contact">
                    <li>
                        <a class="header-phone-icon" href="#">
                            <svg class="ico-svg" viewBox="0 0 21 19" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon-phone" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </a>
                        <a class="header-phone-text" href="tel:+74956617158">+7 (495) 661-71-58</a>
                    </li>
                    <li>
                        <a class="header-callback" href="#modal-callback">Заказать звонок</a>
                    </li>
                </ul>
            </li>

            <li>

                <ul class="header-address">
                    <li>м. Войковская</li>
                    <li><b>пн-пт:</b> 9:00 – 18:00</li>
                </ul>

            </li>

            <li>
                <a class="header-manager" href="#">Бесплатный вызов менеджера</a>
            </li>

            <li>
                <a class="header-enter" href="#">
                    <svg class="ico-svg" viewBox="0 0 23 27">
                        <use xlink:href="img/sprite-icons.svg#icon-door"></use>
                    </svg>
                    <div class="header-dropdown">Личный кабинет</div>
                </a>
            </li>

            <li>
                <a class="header-favorites" href="#">
                    <i>
                        <svg class="ico-svg" viewBox="0 0 26 24">
                            <use xlink:href="img/sprite-icons.svg#icon-heart"></use>
                        </svg>
                        <span class="header-favorites-value">3</span>
                    </i>
                    <div class="header-dropdown">Избранное</div>
                </a>
            </li>

            <li class="header-card-box">
                <div class="header-basket">
                    <a class="header-basket-icon header-basket-toggle" href="#">
                        <i>
                            <svg class="ico-svg" viewBox="0 0 27.5 25.5" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon-cart"></use>
                            </svg>
                            <span class="header-basket-value">3</span>
                        </i>
                    </a>
                    <div class="header-basket-text">
                        <p>Сумма: <strong>167 568</strong> <i class="fa fa-ruble"></i></p>
                        <a href="#">Перейти в корзину</a>
                        <i class="icon-down header-basket-toggle">
                            <i class="fa fa-chevron-down"></i>
                        </i>
                    </div>
                </div>

                <div class="mini-cart-bg"></div>

                <div class="mini-cart">

                    <div class="mini-cart-header">
                        <div class="mini-cart-icon">
                            <svg class="ico-svg" viewBox="0 0 27.5 25.5" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite-icons.svg#icon-cart"></use>
                            </svg>
                            <span class="mini-cart-value">3</span>
                        </div>
                        <div class="mini-cart-text">Сумма: <strong>167 568</strong> <i class="fa fa-ruble"></i></div>
                        <a class="mini-cart-btn" href="#">Перейти в корзину</a>
                        <span class="mini-cart-close">
                            <i class="fa fa-chevron-right"></i>
                        </span>
                    </div>

                    <div class="mini-card-content">
                        <div class="mini-card-wrap">
                            <div class="mini-card-scroll">

                                <div class="mini-card-item">
                                    <span class="card-item-close"></span>
                                    <ul class="mini-card-row">
                                        <li>
                                            <a href="#" class="mini-card-image">
                                                <img src="images/product_backet_01.jpg" class="img-responsive" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a class="mini-card-name" href="#">Стул Cappuccino chrome</a>
                                        </li>
                                        <li>
                                            <div class="form-number">
                                                <span class="minus">-</span>
                                                <span class="plus">+</span>
                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                            </div>
                                        </li>
                                        <li>
                                            <a href="#" class="mini-card-color">
                                                <span class="product-color-type"><img src="img/color_brown.jpg" alt=""></span>
                                                <span class="product-color-name">Кожа орех / коричневая</span>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="mini-card-price">
                                                <span class="mini-card-price-source">3 209 <i class="fa fa-ruble"></i> c х 2 </span>
                                                <span class="mini-card-price-new">5 298 <i class="fa fa-ruble"></i></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="mini-card-item">
                                    <span class="card-item-close"></span>
                                    <ul class="mini-card-row">
                                        <li>
                                            <a href="#" class="mini-card-image">
                                                <img src="images/product_backet_01.jpg" class="img-responsive" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a class="mini-card-name" href="#">Стул Cappuccino chrome</a>
                                        </li>
                                        <li>
                                            <div class="form-number">
                                                <span class="minus">-</span>
                                                <span class="plus">+</span>
                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                            </div>
                                        </li>
                                        <li>
                                            <a href="#" class="mini-card-color">
                                                <span class="product-color-type"><img src="img/color_brown.jpg" alt=""></span>
                                                <span class="product-color-name">Кожа орех / коричневая</span>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="mini-card-price">
                                                <span class="mini-card-price-source">3 209 <i class="fa fa-ruble"></i> c х 2 </span>
                                                <span class="mini-card-price-new">5 298 <i class="fa fa-ruble"></i></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="mini-card-item">
                                    <span class="card-item-close"></span>
                                    <ul class="mini-card-row">
                                        <li>
                                            <a href="#" class="mini-card-image">
                                                <img src="images/product_backet_01.jpg" class="img-responsive" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a class="mini-card-name" href="#">Стул Cappuccino chrome</a>
                                        </li>
                                        <li>
                                            <div class="form-number">
                                                <span class="minus">-</span>
                                                <span class="plus">+</span>
                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                            </div>
                                        </li>
                                        <li>
                                            <a href="#" class="mini-card-color">
                                                <span class="product-color-type"><img src="img/color_brown.jpg" alt=""></span>
                                                <span class="product-color-name">Кожа орех / коричневая</span>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="mini-card-price">
                                                <span class="mini-card-price-source">3 209 <i class="fa fa-ruble"></i> c х 2 </span>
                                                <span class="mini-card-price-new">5 298 <i class="fa fa-ruble"></i></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="mini-cart-footer">
                        <button type="submit" class="btn btn-orange">Оформить заказ</button>

                </div>
            </li>

        </ul>

    </div>

</header>