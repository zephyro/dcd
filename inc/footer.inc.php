<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="inner">

                <ul class="tile-top">

                    <li>
                        <div class="h4">Контакты</div>
                        <ul class="contact-phone">
                            <li><a href="tel:+74956617158">+7 (495) 661-71-58</a></li>
                            <li><a href="tel:+74957788319">+7 (495) 778-83-19</a></li>
                            <li><a href="tel:+76972255380">+7 (697) 225-53-80</a></li>
                        </ul>
                        <div class="address">127299, г. Москва, м. Войковская, ул. Космонавта Волкова, д. 10, шоурум №103/2, «Компания ДСД»</div>
                    </li>

                    <li>
                        <div class="h4">Покупателям</div>
                        <ul class="footer-nav">
                            <li><a href="#">Оплата</a></li>
                            <li><a href="#">Гарантия</a></li>
                            <li><a href="#">Спецпредложения</a></li>
                            <li><a href="#">Доставка и сборка</a></li>
                            <li><a href="#">Ремонт мебели</a></li>
                            <li><a href="#">Полезная информация</a></li>
                            <li><a href="#">Словарь терминов</a></li>
                        </ul>
                    </li>

                    <li>
                        <ul class="info-nav">
                            <li><a href="#">Госзаказчикам</a></li>
                            <li><a href="#">Дилерам</a></li>
                            <li><a href="#">Новости</a></li>
                            <li><a href="#">Вакансии</a></li>
                            <li><a href="#">галерея</a></li>
                        </ul>
                    </li>

                    <li>
                        <ul class="contact-social">
                            <li>
                                <div class="h4">Мы в социальных сетях</div>
                                <ul class="social">
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-vk"></i></a></li>
                                    <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="portal-logo"><span>Мы зарегистрированы на портале поставщиков</span></a>
                            </li>
                        </ul>
                    </li>

                    <li>

                        <ul class="footer-buttons">
                            <li><a class="btn btn-light" href="#">Связаться с нами</a></li>
                            <li><a class="btn btn-light" href="#">Вызвать менеджера</a></li>
                        </ul>

                        <ul class="footer-subscribe">
                            <li>
                                <div class="subscribe-title">Подпишитесь на рассылку новостей:</div>
                            </li>
                            <li>
                                <div class="subscribe-form">
                                    <form class="form">
                                        <input class="subscribe-input" type="text" placeholder="Введите е-mail">
                                        <button class="subscribe-btn" type="submit">
                                            <svg class="ico-svg" viewBox="0 0 24 22" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-plane" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="inner">
                <ul class="tile-bottom">
                    <li>
                        <div class="footer-copy">
                            Компания ДСД: продажа офисной мебели
                            <span>1995 – 2017 © copyright dcd.ru</span>
                        </div>
                    </li>
                    <li>
                        <div class="footer-rate">
                            <span>Оцените качество работы компании:</span>
                            <div class="rateStars-input">
                                <input id="star-4" type="radio" name="reviewStars"/>
                                <label title="gorgeous" for="star-4"><i class="fa fa-star"></i></label>

                                <input id="star-3" type="radio" name="reviewStars"/>
                                <label title="good" for="star-3"><i class="fa fa-star"></i></label>

                                <input id="star-2" type="radio" name="reviewStars"/>
                                <label title="regular" for="star-2"><i class="fa fa-star"></i></label>

                                <input id="star-1" type="radio" name="reviewStars"/>
                                <label title="poor" for="star-1"><i class="fa fa-star"></i></label>

                                <input id="star-0" type="radio" name="reviewStars"/>
                                <label title="bad" for="star-0"><i class="fa fa-star"></i></label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a class="footer-dev">Дизайн и разработка сайта:</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<!-- SVG -->

<div class="hide">
    <?php include('img/heart.svg') ?>
</div>


<!-- Модальное окно -  Выбор цвета -->
<div class="hide">
    <div class="modal modal-color" id="modal-color">
        <form class="form">
            <div class="modal-header">
                <h4>Выберите цвет</h4>
            </div>
            <div class="modal-body">
                <div class="content-scroll">
                    <div class="modal-content">
                        <div class="colors-box">
                            <div class="color-head">Тканевые</div>
                            <p>Недорогая и тонкая, но практичная искусственная кожа, очень хорошо подходит для изделий имеющих складки, типа Premier</p>
                            <ul class="color-list">
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_01.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_02.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_03.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_01.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_02.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_03.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_01.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_02.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_03.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_01.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_02.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_03.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_02.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_03.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="colors-box">
                            <div class="color-head">Эко-кожа</div>
                            <p>Недорогая и тонкая, но практичная искусственная кожа, очень хорошо подходит для изделий имеющих складки, типа Premier</p>
                            <ul class="color-list">
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_01.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_02.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_03.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_01.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_02.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_03.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_01.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_02.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_03.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_01.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_02.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_03.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_02.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input class="checkbox" type="checkbox" name="checkbox-color">
                                        <span class="checkbox-custom">
                                            <img src="images/colors_lg/color_03.jpg" class="img-responsive" alt="">
                                        </span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <ul>
                    <li><span>Выбрано цветов:  5</span></li>
                    <li><a href="#" class="btn btn-orange">Подтвердить</a></li>
                </ul>
            </div>
        </form>
    </div>
</div>
<!-- -->