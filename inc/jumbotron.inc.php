<section class="jumbotron">
    <div class="container">
        <div class="jumbotron-inner">
            <div class="jumbotron-wrap">
                <div class="jumbotron-image">
                    <div class="jumbotron-image-slider">
                        <div class="slider-item">
                            <img src="img/slider_image_one.jpg" class="img-responsive" alt="">
                        </div>
                        <div class="slider-item">
                            <img src="img/slider_image_one.jpg" class="img-responsive" alt="">
                        </div>
                        <div class="slider-item">
                            <img src="img/slider_image_one.jpg" class="img-responsive" alt="">
                        </div>
                    </div>
                </div>

                <div class="jumbotron-slider-wrap">
                    <div class="jumbotron-slider">

                        <div class="jumbotron-slider-item">
                            <div class="slider-item-row">
                                <div class="slider-item-col">
                                    <div class="h2">На сайте дешевле</div>
                                    <div class="text">На все заказы, оформленные через корзину, действуют скидки! Подробнее на странице получить скидку</div>
                                    <a href="#" class="btn">Подробнее</a>
                                </div>
                            </div>
                        </div>

                        <div class="jumbotron-slider-item">
                            <div class="slider-item-row">
                                <div class="slider-item-col">
                                    <div class="h2">На сайте дешевле</div>
                                    <div class="text">На все заказы, оформленные через корзину, действуют скидки! Подробнее на странице получить скидку</div>
                                    <a href="#" class="btn">Подробнее</a>
                                </div>
                            </div>
                        </div>

                        <div class="jumbotron-slider-item">
                            <div class="slider-item-row">
                                <div class="slider-item-col">
                                    <div class="h2">На сайте дешевле</div>
                                    <div class="text">На все заказы, оформленные через корзину, действуют скидки! Подробнее на странице получить скидку</div>
                                    <a href="#" class="btn">Подробнее</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>