<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Дилерам</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Дилерам</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->


                    <!-- Main content -->
                    <div class="content content-overflow">
                        <div class="container">
                            <div class="border-wrap">
                                <div class="text">Компания ДСД приглашает к сотрудничеству торговые компании и региональные представительства. Наш, более чем 20-летний опыт работы на мебельном рынке, позволяет держать конкурентоспособные цены и предоставлять значительные скидки торговым компаниям на весь ассортиментный ряд продукции.</div>
                            </div>
                            <div class="heading-gray">
                                <div class="border-wrap">
                                    <h4>Выгода для наших партнёров</h4>
                                </div>
                            </div>
                            <div class="border-wrap">
                                <ul class="benefit">
                                    <li>
                                        <div class="benefit-row">
                                            <div class="benefit-col">
                                                <div class="benefit-icon">
                                                    <svg class="ico-svg" viewBox="0 0 70 48" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite-icons.svg#icon-adv-hand" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <h4><span>Сборка</span></h4>
                                                <p>Экономьте свое время с нашими сборщиками</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="benefit-row">
                                            <div class="benefit-col">
                                                <div class="benefit-icon">
                                                    <svg class="ico-svg" viewBox="0 0 41 47" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite-icons.svg#icon-adv-service" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <h4><span>Персональный менеджер</span></h4>
                                                <p>Эгарантия внимательной обработки каждого заказа, помощь и консультации</p>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="benefit-row">
                                            <div class="benefit-col">
                                                <div class="benefit-icon">
                                                    <svg class="ico-svg" viewBox="0 0 39 53" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite-icons.svg#icon-opt" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <h4><span>Оптовые цены</span></h4>
                                                <p>при заключении дилерского договора каждой компании предоставляются специальные цены</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="benefit-row">
                                            <div class="benefit-col">
                                                <div class="benefit-icon">
                                                    <svg class="ico-svg" viewBox="0 0 37 44" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite-icons.svg#icon-enter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <h4><span>Личный кабинет </span></h4>
                                                <p>при авторизации в личном кабинете со статусом Дилер доступны оптовые цены на всю продукцию</p>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="benefit-row">
                                            <div class="benefit-col">
                                                <div class="benefit-icon">
                                                    <svg class="ico-svg" viewBox="0 0 56 45" xmlns="http://www.w3.org/2000/svg">
                                                        <use xlink:href="img/sprite-icons.svg#icon-delay" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                    </svg>
                                                </div>
                                                <h4><span>Отсрочка платежа</span></h4>
                                                <p>по результатам работы предоставляется кредит</p>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                            <div class="heading-gray">
                                <div class="border-wrap">
                                    <h4>Чтобы подать заявку на заключение Договора дилера и получить доступ к оптовым ценам необходимо:</h4>
                                </div>
                            </div>
                            <div class="border-wrap">
                                <ol class="dealers-step">
                                    <li><a class="icon-pdf" href="#"><span>Скачать</span></a> и заполнить Анкету дилера</li>
                                    <li>Отправить её на <a href="#">электронную почту</a></li>
                                    <li>Получить доступ к оптовым ценам</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- -->

                </section>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- script -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>

