<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">
                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Вакансии</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Вакансии</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <!-- Nav heading -->
                    <nav class="heading-nav">
                        <div class="container">
                            <ul class="tabs-nav">
                                <li class="active"><a href="#" data-target=".tab01">Наши вакансии</a></li>
                                <li><a href="#" data-target=".tab02">Отправить резюме</a></li>
                                <li><a href="#" data-target=".tab03">Сотрудничество</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- -->

                    <div class="content content-overflow">
                        <div class="container">
                            <div class="border-wrap">

                                <div class="tabs">

                                    <div class="tabs-item tab01 active">
                                        <ul class="vacancy">
                                            <li>
                                                <div class="vacancy-title">
                                                    <a href="#">Менеджер по продажам</a>
                                                    <span class="vacancy-toggle"></span>
                                                </div>
                                                <div class="vacancy-content">
                                                    <h4>Требования:</h4>
                                                    <p>Мужчина или женщина от 18 до 25 лет. Личные качества: коммуникабельность, пунктуальность, грамотная речь, желание и стремление к обучению, нацеленность на результат, готовность работать в команде. Гражданство Россия.</p>
                                                    <h4>Обязанности:</h4>
                                                    <p>Приобретение навыков работы с мебельной продукцией, изучение и продвижение ассортимента, добросовестное выполнение служебных обязанностей по реализации проектов.</p>
                                                    <h4>Условия:</h4>
                                                    <p>График работы: 5\2 с 9.00 до 18.00 (перерыв 1 час). Работа в офисе, выезды к заказчикам по Москве. Зарплата: оклад + премия + выслуга лет. Индексация зарплаты 2 раза в год. Оплачивается служебный проезд и сотовая связь.</p>
                                                    <h4>Контактное лицо:</h4>
                                                    <p>Сергей Анатольевич (<a href="mailto:info@dcd.ru">info@dcd.ru</a>)</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="vacancy-title">
                                                    <a href="#">Помощник менеджера по продажам</a>
                                                    <span class="vacancy-toggle"></span>
                                                </div>
                                                <div class="vacancy-content">
                                                    <h4>Требования:</h4>
                                                    <p>Мужчина или женщина от 18 до 25 лет. Личные качества: коммуникабельность, пунктуальность, грамотная речь, желание и стремление к обучению, нацеленность на результат, готовность работать в команде. Гражданство Россия.</p>
                                                    <h4>Обязанности:</h4>
                                                    <p>Приобретение навыков работы с мебельной продукцией, изучение и продвижение ассортимента, добросовестное выполнение служебных обязанностей по реализации проектов.</p>
                                                    <h4>Условия:</h4>
                                                    <p>График работы: 5\2 с 9.00 до 18.00 (перерыв 1 час). Работа в офисе, выезды к заказчикам по Москве. Зарплата: оклад + премия + выслуга лет. Индексация зарплаты 2 раза в год. Оплачивается служебный проезд и сотовая связь.</p>
                                                    <h4>Контактное лицо:</h4>
                                                    <p>Сергей Анатольевич (<a href="mailto:info@dcd.ru">info@dcd.ru</a>)</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="vacancy-title">
                                                    <a href="#">Сборщик мебели</a>
                                                    <span class="vacancy-toggle"></span>
                                                </div>
                                                <div class="vacancy-content">
                                                    <h4>Требования:</h4>
                                                    <p>Мужчина или женщина от 18 до 25 лет. Личные качества: коммуникабельность, пунктуальность, грамотная речь, желание и стремление к обучению, нацеленность на результат, готовность работать в команде. Гражданство Россия.</p>
                                                    <h4>Обязанности:</h4>
                                                    <p>Приобретение навыков работы с мебельной продукцией, изучение и продвижение ассортимента, добросовестное выполнение служебных обязанностей по реализации проектов.</p>
                                                    <h4>Условия:</h4>
                                                    <p>График работы: 5\2 с 9.00 до 18.00 (перерыв 1 час). Работа в офисе, выезды к заказчикам по Москве. Зарплата: оклад + премия + выслуга лет. Индексация зарплаты 2 раза в год. Оплачивается служебный проезд и сотовая связь.</p>
                                                    <h4>Контактное лицо:</h4>
                                                    <p>Сергей Анатольевич (<a href="mailto:info@dcd.ru">info@dcd.ru</a>)</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="vacancy-title">
                                                    <a href="#">Водитель-экспедитор</a>
                                                    <span class="vacancy-toggle"></span>
                                                </div>
                                                <div class="vacancy-content">
                                                    <h4>Требования:</h4>
                                                    <p>Мужчина или женщина от 18 до 25 лет. Личные качества: коммуникабельность, пунктуальность, грамотная речь, желание и стремление к обучению, нацеленность на результат, готовность работать в команде. Гражданство Россия.</p>
                                                    <h4>Обязанности:</h4>
                                                    <p>Приобретение навыков работы с мебельной продукцией, изучение и продвижение ассортимента, добросовестное выполнение служебных обязанностей по реализации проектов.</p>
                                                    <h4>Условия:</h4>
                                                    <p>График работы: 5\2 с 9.00 до 18.00 (перерыв 1 час). Работа в офисе, выезды к заказчикам по Москве. Зарплата: оклад + премия + выслуга лет. Индексация зарплаты 2 раза в год. Оплачивается служебный проезд и сотовая связь.</p>
                                                    <h4>Контактное лицо:</h4>
                                                    <p>Сергей Анатольевич (<a href="mailto:info@dcd.ru">info@dcd.ru</a>)</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="vacancy-title">
                                                    <a href="#">Дизайнер мебельного интерьера</a>
                                                    <span class="vacancy-toggle"></span>
                                                </div>
                                                <div class="vacancy-content">
                                                    <h4>Требования:</h4>
                                                    <p>Мужчина или женщина от 18 до 25 лет. Личные качества: коммуникабельность, пунктуальность, грамотная речь, желание и стремление к обучению, нацеленность на результат, готовность работать в команде. Гражданство Россия.</p>
                                                    <h4>Обязанности:</h4>
                                                    <p>Приобретение навыков работы с мебельной продукцией, изучение и продвижение ассортимента, добросовестное выполнение служебных обязанностей по реализации проектов.</p>
                                                    <h4>Условия:</h4>
                                                    <p>График работы: 5\2 с 9.00 до 18.00 (перерыв 1 час). Работа в офисе, выезды к заказчикам по Москве. Зарплата: оклад + премия + выслуга лет. Индексация зарплаты 2 раза в год. Оплачивается служебный проезд и сотовая связь.</p>
                                                    <h4>Контактное лицо:</h4>
                                                    <p>Сергей Анатольевич (<a href="mailto:info@dcd.ru">info@dcd.ru</a>)</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="tabs-item tab02">

                                        <div class="form-brief">
                                            <form class="form">
                                                <h4>Личные данные</h4>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Желаемая должность</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">ФИО</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Пол</label>
                                                    </li>
                                                    <li>
                                                        <label class="form-radio">
                                                            <input class="input-radio" type="radio" name="sex" value="male" checked>
                                                            <span class="form-radio-icon"></span>
                                                            <span class="form-radio-text">Мужской</span>
                                                        </label>
                                                        <label class="form-radio">
                                                            <input class="input-radio" type="radio" name="sex" value="female">
                                                            <span class="form-radio-icon"></span>
                                                            <span class="form-radio-text">Женский</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Возраст</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-sm" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Гражданство</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-md" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Образование</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-md" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Место жительства</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Стаж работы по специальности</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-sm" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Желаемая з/п</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control form-control-sm" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Цель работы</label>
                                                    </li>
                                                    <li>
                                                        <textarea class="form-control" name="message" placeholder="" rows="4"></textarea>
                                                    </li>
                                                </ul>

                                                <h4>Контакты</h4>

                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">E-mail</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Телефон</label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label"></label>
                                                    </li>
                                                    <li>
                                                        <input type="text" class="form-control" name="" placeholder="">
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Пару слов о себе</label>
                                                    </li>
                                                    <li>
                                                        <textarea class="form-control" name="message" placeholder="" rows="4"></textarea>
                                                    </li>
                                                </ul>
                                                <ul class="form-row">
                                                    <li>
                                                        <label class="form-label">Прикрепите файл</label>
                                                    </li>
                                                    <li>
                                                        <button class="btn">Прикрепите файл</button>
                                                    </li>
                                                </ul>
                                                <div class="form-action">
                                                    <button type="submit" class="btn btn-orange btn-caps">отправить</button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>

                                    <div class="tabs-item tab03">

                                        <div class="partners-title">Информация для сотрудничества на основе аутсёрсинга и партнёрства.</div>
                                        <div class="partners-content">
                                            <h4>Транспорт</h4>
                                            <p>Приглашаем к сотрудничеству надёжные транспортные компании с опытом работы. Конкурентные цены на услуги, включающие НДС обязательны. Оплата по безналичному расчёту по итогам оговоренного периода. Предоставление детального отчёта (акта) о работе транспорта. Используемый нами транспорт грузоподъёмностью от 1 тонны до 5 тонн. Требуется грузолегковой автомобиль для выполнения единоразовых заказов.</p>
                                            <a href="mailto:info@dcd.ru">mailto:info@dcd.ru</a>
                                            <br/><br/>
                                            <h4>Сборка мебели и такелажные работы</h4>
                                            <p>Приглашаем организации и индивидуальных предпринимателей с опытом работы по сборке мебели и собственным профессиональным инструментом. Умение работать с технической документацией и чтение чертежей необходимо. Наличие транспорта и сопутствующего оборудования для выполнения работ.</p>
                                            <a href="mailto:info@dcd.ru">info@dcd.ru</a>
                                            <br/><br/>
                                            <h4>Агентам по продвижению мебельной продукции</h4>
                                            <p>Выплачиваем комиссионное вознаграждение за привлечение покупателей. Предоставляем скидки торговым компаниям для реализации конечному покупателю. Обеспечим рекламной продукцией, образцами и справочной литературой.</p>
                                            <a href="mailto:info@dcd.ru">info@dcd.ru</a>
                                            <br/><br/>
                                            <h4>Дизайн и проектирование</h4>
                                            <p>Окажем помощь дизайнерским бюро и проектировщикам офисных интерьеров в поставке мебели на объекты под ключ. Разместим заказы для совместного исполнения проектов. Оплачиваем услуги по составлению документации.</p>
                                            <a href="mailto:info@dcd.ru">info@dcd.ru</a>
                                            <br/>
                                        </div>
                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>


                </section>
                <!-- -->
            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
