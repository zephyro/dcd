<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Каталог</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Каталог</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <div class="category category-page content-overflow">
                        <div class="container">
                            <ul>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_01.jpg" class="img-responsive" alt="">
                                        <div class="category-text">Мебель для руководителя</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_02.jpg" class="img-responsive" alt="">
                                        <div class="category-text">Мебель для персонала</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_03.jpg" class="img-responsive" alt="">
                                        <div class="category-text">Ресепшн</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_04.jpg" class="img-responsive" alt="">
                                        <div class="category-text">офисные кресла и стулья</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_05.jpg" class="img-responsive" alt="">
                                        <div class="category-text">Мебель для компьютера</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_06.jpg" class="img-responsive" alt="">
                                        <div class="category-text">мини-кухни и шкафы</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_07.jpg" class="img-responsive" alt="">
                                        <div class="category-text">мягкая офисная мебель</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_08.jpg" class="img-responsive" alt="">
                                        <div class="category-text">Переговорная зона</div>
                                    </a>
                                </li>

                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_09.jpg" class="img-responsive" alt="">
                                        <div class="category-text">Мебель специальная</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_10.jpg" class="img-responsive" alt="">
                                        <div class="category-text">мебель для гостиниц</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_11.jpg" class="img-responsive" alt="">
                                        <div class="category-text">мебель на заказ</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_12.jpg" class="img-responsive" alt="">
                                        <div class="category-text">мебель для дома</div>
                                    </a>
                                </li>

                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_13.jpg" class="img-responsive" alt="">
                                        <div class="category-text">школьная мебель</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_14.jpg" class="img-responsive" alt="">
                                        <div class="category-text">медицинская мебель</div>
                                    </a>
                                </li>
                                <li>
                                    <a class="category-item" href="#">
                                        <img src="images/category_15.jpg" class="img-responsive" alt="">
                                        <div class="category-text">сопутствующие товары</div>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>

                    <!-- Main content -->
                    <div class="content content-overflow">
                        <div class="container">
                            <div class="border-wrap">
                                <div class="content-box">
                                    <h3>Купите офисную мебель в интернет магазине "ДСД"!</h3>
                                    <p>Уважаемые посетители, в интернет магазине "ДСД" Вам представляется возможность оформить заказ офисной мебели российских и зарубежных производителей, кресла, стулья и другие атрибуты, необходимые для обустройства площадей и создания комфортной рабочей атмосферы в офисном помещении.</p>
                                    <p>В нашем интернет магазине Вы можете ознакомиться с предлагаемыми сериями мебели для кабинета, цветовыми решениями и описаниями продукции. Вы можете добавить понравившиеся модели рабочей и компьютерной мебели в свою корзину и отправить заказ сотрудникам нашего сайта.</p>

                                    <h4>Этим интересуются:</h4>
                                    <ul>
                                        <li>Серия мебели <a href="#">Арго</a> – один из лучших вариантов для обстановки офиса или рабочего кабинета.</li>
                                        <li>Предлагаем качественную мебель эконом-класса <a href="#">для гостиниц</a> и отелей.</li>
                                        <li>Эффектные и современные <a href="#">напольные вешалки</a> для одежды серии Галилео.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- -->

                </section>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
