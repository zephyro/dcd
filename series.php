<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Мебель для руководителей</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <div class="heading-wrap">
                                    <h1>Мебель для руководителей</h1>
                                    <ul class="heading-product-cat">
                                        <li><a href="#budget" class="btn-scroll">Бюджет</a></li>
                                        <li><a href="#economy" class="btn-scroll">Эконом</a></li>
                                        <li><a href="#business" class="btn-scroll">Бизнес</a></li>
                                        <li><a href="#premium" class="btn-scroll">Премиум</a></li>
                                    </ul>
                                </div>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->


                    <div class="sort sort-white">
                        <div class="container">
                            <div class="border-wrap">
                                <div class="clearfix">
                                    <div class="sort-group-left">
                                        <span class="sort-legend">Сортировать:</span>
                                        <ul class="sort-elem">
                                            <li>По цене</li>
                                            <li>
                                                <label class="sort-item sort-item-down">
                                                    <input type="radio" class="sort-radio" name="price">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-down"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                                <label class="sort-item sort-item-up">
                                                    <input type="radio" class="sort-radio" name="price">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-up"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                        <ul class="sort-elem">
                                            <li>По популярности</li>
                                            <li>
                                                <label class="sort-item sort-item-down">
                                                    <input type="radio" class="sort-radio" name="pop">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-down"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                                <label class="sort-item sort-item-up">
                                                    <input type="radio" class="sort-radio" name="pop">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7" name="pop">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-up"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="second-heading" id="budget">
                        <div class="container">
                            <div class="border-wrap">
                                <h3>Бюджет</h3>
                            </div>
                        </div>
                    </div>

                    <!-- Main content -->
                    <div class="content content-overflow">
                        <div class="container">
                            <div class="border-wrap">

                                <ul class="showcase-custom">
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_16.jpg" class="img-responsive" alt="">
                                            <h5>Шкафы купе</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_17.jpg" class="img-responsive" alt="">
                                            <h5>Гардеробные</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_18.jpg" class="img-responsive" alt="">
                                            <h5>Стойки ресепшн</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_18.jpg" class="img-responsive" alt="">
                                            <h5>Стойки ресепшн</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>

                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_16.jpg" class="img-responsive" alt="">
                                            <h5>Шкафы купе</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_17.jpg" class="img-responsive" alt="">
                                            <h5>Гардеробные</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_18.jpg" class="img-responsive" alt="">
                                            <h5>Стойки ресепшн</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_18.jpg" class="img-responsive" alt="">
                                            <h5>Стойки ресепшн</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>

                                </ul>

                            </div>

                        </div>
                    </div>
                    <!-- -->


                    <div class="second-heading" id="economy">
                        <div class="container">
                            <div class="border-wrap">
                                <h3>Эконом</h3>
                            </div>
                        </div>
                    </div>

                    <!-- Main content -->
                    <div class="content content-overflow">
                        <div class="container">
                            <div class="border-wrap">

                                <ul class="showcase-custom">
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_16.jpg" class="img-responsive" alt="">
                                            <h5>Шкафы купе</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_17.jpg" class="img-responsive" alt="">
                                            <h5>Гардеробные</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_18.jpg" class="img-responsive" alt="">
                                            <h5>Стойки ресепшн</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_18.jpg" class="img-responsive" alt="">
                                            <h5>Стойки ресепшн</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>

                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_16.jpg" class="img-responsive" alt="">
                                            <h5>Шкафы купе</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_17.jpg" class="img-responsive" alt="">
                                            <h5>Гардеробные</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_18.jpg" class="img-responsive" alt="">
                                            <h5>Стойки ресепшн</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="showcase-item" href="#">
                                            <img src="images/category_18.jpg" class="img-responsive" alt="">
                                            <h5>Стойки ресепшн</h5>
                                            <ul class="showcase-info">
                                                <li>
                                                    <span>Цена:</span>
                                                    <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                </li>
                                                <li>
                                                    <span>Срок поставки:</span>
                                                    <strong>на складе</strong>
                                                </li>
                                            </ul>
                                        </a>
                                    </li>

                                </ul>

                            </div>

                        </div>
                    </div>
                    <!-- -->

                </section>
                <!-- -->

                <!-- Leaders block -->
                <?php include('inc/leaders.inc.php') ?>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- script -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
