<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Slider -->
                <?php include('inc/jumbotron.inc.php') ?>
                <!-- -->

                <section class="advantage">
                    <div class="container">
                        <ul>
                            <li>
                                <a href="#" class="advantage-item">
                                    <div class="advantage-icon">
                                        <svg class="ico-svg" viewBox="0 0 70 48" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite-icons.svg#icon-adv-hand" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <h4><span>Опыт и надежность</span></h4>
                                    <p>Опыт работы более 20 лет</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="advantage-item">
                                    <div class="advantage-icon">
                                        <svg class="ico-svg" viewBox="0 0 41 47" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite-icons.svg#icon-adv-service" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <h4><span>Клиентский сервис</span></h4>
                                    <p>От дизайн проекта до инсталяции интерьера</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="advantage-item">
                                    <div class="advantage-icon">
                                        <svg class="ico-svg" viewBox="0 0 63 47" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite-icons.svg#icon-adv-shipping" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <h4><span>Гибкие тарифы доставки</span></h4>
                                    <p>Доставка от 300 руб на сегодня</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="advantage-item">
                                    <div class="advantage-icon">
                                        <svg class="ico-svg" viewBox="0 0 44 49" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite-icons.svg#icon-adv-term" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <h4><span>Индивидуальные ценовые условия</span></h4>
                                    <p>Обсуждаем скидки</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </section>

                <section class="category">
                    <div class="container">
                        <ul>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_01.jpg" class="img-responsive" alt="">
                                    <div class="category-text">Мебель для руководителя</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_02.jpg" class="img-responsive" alt="">
                                    <div class="category-text">Мебель для персонала</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_03.jpg" class="img-responsive" alt="">
                                    <div class="category-text">Ресепшн</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_04.jpg" class="img-responsive" alt="">
                                    <div class="category-text">офисные кресла и стулья</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_05.jpg" class="img-responsive" alt="">
                                    <div class="category-text">Мебель для компьютера</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_06.jpg" class="img-responsive" alt="">
                                    <div class="category-text">мини-кухни и шкафы</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_07.jpg" class="img-responsive" alt="">
                                    <div class="category-text">мягкая офисная мебель</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_08.jpg" class="img-responsive" alt="">
                                    <div class="category-text">Переговорная зона</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_09.jpg" class="img-responsive" alt="">
                                    <div class="category-text">Мебель специальная</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_10.jpg" class="img-responsive" alt="">
                                    <div class="category-text">мебель для гостиниц</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_11.jpg" class="img-responsive" alt="">
                                    <div class="category-text">мебель на заказ</div>
                                </a>
                            </li>
                            <li>
                                <a class="category-item" href="#">
                                    <img src="images/category_12.jpg" class="img-responsive" alt="">
                                    <div class="category-text">мебель для дома</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </section>

                <section class="goods">
                    <div class="container">
                        <div class="inner">
                            <div class="goods-heading"><a href="#">Все товары</a></div>
                            <div class="goods-slider">
                                <div class="goods-slider-item">
                                    <a class="goods-item" href="#">
                                        <div class="goods-icon">
                                            <svg class="ico-svg" viewBox="0 0 42 71" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-product-chair" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <h4>Стулья</h4>
                                        <p>1 523 товара</p>
                                    </a>
                                </div>
                                <div class="goods-slider-item">
                                    <a class="goods-item" href="#">
                                        <div class="goods-icon">
                                            <svg class="ico-svg" viewBox="0 0 79 73" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-product-table" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <h4>Столы</h4>
                                        <p>594 товара</p>
                                    </a>
                                </div>
                                <div class="goods-slider-item">
                                    <a class="goods-item" href="#">
                                        <div class="goods-icon">
                                            <svg class="ico-svg" viewBox="0 0 60 72" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-product-pedestal" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <h4>Тумбы</h4>
                                        <p>134 товара</p>
                                    </a>
                                </div>
                                <div class="goods-slider-item">
                                    <a class="goods-item" href="#">
                                        <div class="goods-icon">
                                            <svg class="ico-svg" viewBox="0 0 52 72" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-product-wardrobe" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <h4>Шкафы</h4>
                                        <p>95 товаров</p>
                                    </a>
                                </div>
                                <div class="goods-slider-item">
                                    <a class="goods-item" href="#">
                                        <div class="goods-icon">
                                            <svg class="ico-svg" viewBox="0 0 79 73" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-product-table" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </div>
                                        <h4>Столы</h4>
                                        <p>594 товара</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="news-block">
                    <div class="container">
                        <div class="inner">
                            <h3>Новости</h3>

                            <ul>
                                <li>
                                    <a class="news-item" href="#">
                                        <div class="news-image">
                                            <img src="images/news_01.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Моно-Люкс - цены снижены!</h4>
                                            <p>Мебель для персонала Моно-Люкс теперь еще выгоднее! Новые привлекательные цены в нашем интернет-магазине!</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="news-item hidden-xs hidden-sm" href="#" >
                                        <div class="news-image">
                                            <img src="images/news_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Мебель Альтернатива - в каталоге!</h4>
                                            <p>Наш каталог пополнен новой серией мебели Альтернатива! Серия участвует в проходящей акции по фабрике Экспро!</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="news-item hidden-xs hidden-sm hidden-md" href="#">
                                        <div class="news-image">
                                            <img src="images/news_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Ресепшн Ринг доступен для заказа!</h4>
                                            <p>Ресепшн Ринг - доступный и стильный вариант ресепшн для современного офиса! Необычный дизайн угловым...</p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a class="news-total" href="#">
                                        <span>Все новости</span>
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </section>

                <!-- Leaders block -->
                <?php include('inc/leaders.inc.php') ?>
                <!-- -->

                <section class="variety">
                    <div class="container">
                        <div class="inner">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="variety-text">
                                        <h2>Как выбрать и где купить офисную мебель в Москве</h2>
                                        <p>Вы решили переоборудовать помещение офиса? Или, можетбыть, вам предстоит открывать новый филиал фирмы? Планируете купить офисную мебель и подыскиваете хороший магазин офисной мебели в Москве? </p>
                                        <p>Тогда вам надо знать, что мебель для офиса играет важную роль в создании имиджа организации. От нее зависит психологический комфорт как посетителей, так и сотрудников фирмы. Для выбора нет необходимости посещать салон  офисной мебели в Москве, сегодня достаточно информации представляет нашинтернет-магазин по продаже офисной мебели. Итак, выбираем новую мебель в офис.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="variety-image">
                                        <div class="variety-image-inner">
                                            <img src="images/article_image.jpg" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <ul class="variety-tiles">
                                <li>
                                    <h4>Выбираем офисные кресла и стулья</h4>
                                    <p>Офисные кресла и стулья принято условно делить на три группы:</p>
                                </li>
                                <li>
                                    <div class="variety-title"><a href="#">Кресла для руководителя</a></div>
                                    <p>самые дорогие и технически оснащенные. Они массивнее, выше и представительнее других стульев и кресел. Если пространство офиса просматривается полностью оно возвышается над всеми, подтверждая статус своего хозяина. </p>
                                </li>
                                <li>
                                    <div class="variety-title"><a href="#">Офисные стулья и кресла</a></div>
                                    <p>для сотрудников отличаются своей простотой и удобством. Для того, чтобы человек мог в течение целого дня сохранять работоспособность, его рабочее место должно быть продумано до мелочей и сохранять комфорт своему владельцу.</p>
                                </li>
                                <li>
                                    <div class="variety-title"><a href="#">Кресла и стулья посетителей</a></div>
                                    <p>различны по дизайну и обычно соответствуют общему стилевому решению офиса. В нашем каталоге Вы найдете большое разнообразие офисных кресел и стульев, сможете подобрать по внешнему виду, цветовому решению и цене.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>

                <section class="vendors">
                    <div class="container">
                        <div class="inner">
                            <div class="vendors-heading"><span>Наши клиенты</span></div>
                            <div class="vendors-slider">
                                <div class="vendors-slider-item">
                                    <div class="vendor">
                                        <div class="vendor-image">
                                            <img src="images/vendor_logo_01.png" alt="">
                                        </div>
                                        <div class="vendor-text"><span>Управление ЗАГС г. Москвы</span></div>
                                    </div>
                                </div>
                                <div class="vendors-slider-item">
                                    <div class="vendor">
                                        <div class="vendor-image">
                                            <img src="images/vendor_logo_02.png" alt="">
                                        </div>
                                        <div class="vendor-text"><span>МГТУ им. Н.Э. Баумана</span></div>
                                    </div>
                                </div>
                                <div class="vendors-slider-item">
                                    <div class="vendor">
                                        <div class="vendor-image">
                                            <img src="images/vendor_logo_03.png" alt="">
                                        </div>
                                        <div class="vendor-text"><span>ФКУ ФМС  России</span></div>
                                    </div>
                                </div>
                                <div class="vendors-slider-item">
                                    <div class="vendor">
                                        <div class="vendor-image">
                                            <img src="images/vendor_logo_04.png" alt="">
                                        </div>
                                        <div class="vendor-text"><span>Аэропорт Шереметьево</span></div>
                                    </div>
                                </div>
                                <div class="vendors-slider-item">
                                    <div class="vendor">
                                        <div class="vendor-image">
                                            <img src="images/vendor_logo_01.png" alt="">
                                        </div>
                                        <div class="vendor-text"><span>Управление ЗАГС г. Москвы</span></div>
                                    </div>
                                </div>
                                <div class="vendors-slider-item">
                                    <div class="vendor">
                                        <div class="vendor-image">
                                            <img src="images/vendor_logo_02.png" alt="">
                                        </div>
                                        <div class="vendor-text"><span>МГТУ им. Н.Э. Баумана</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- script -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
