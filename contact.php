<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Контакты</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Заголовок</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <div class="main-contact">
                        <div class="container">
                            <div class="inner">
                                <div class="contact">
                                    <div class="contact-gallery">
                                        <a data-fancybox="contact-gallery" href="images/contact_slider_image_one.jpg">
                                            <img src="images/contact_slider_image_one.jpg" class="img-responsive" about="">
                                        </a>
                                        <a data-fancybox="contact-gallery" href="images/contact_slider_image_one.jpg">
                                            <img src="images/contact_slider_image_one.jpg" class="img-responsive" about="">
                                        </a>
                                    </div>
                                    <div class="contact-body">
                                        <div class="contact-title">Наш адрес:</div>
                                        <p>127299, г. Москва, ул. Космонавта Волкова, д. 10, шоурум № 103/2</p>
                                        <div class="contact-title">Ближайшее метро:</div>
                                        <p>Войковская</p>
                                        <div class="contact-title">Телефон:</div>
                                        <p>+7 (495) 661-71-58</p>
                                        <div class="contact-title">Режим работы:</div>
                                        <p>
                                            Пн-Чт — 09:00-18:00<br/>
                                            Пт — 09:00-17:00<br/>
                                            Сб-Вс — выходной
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="contact-map">
                            <div class="map-border"></div>
                            <div class="map" id="map"></div>
                        </div>

                        <div class="contact-body">
                            <div class="container">

                                <div class="contact-row">
                                    <div class="contact-left">
                                        <div class="heading-gray">
                                            <div class="border-wrap">
                                                <h4>Варианты проезда</h4>
                                            </div>
                                        </div>
                                        <div class="contact-wrap">
                                            <h5>На метро и пешком:</h5>
                                            <p>м. Войковская, последний вагон из Центра, выход в город налево из стеклянных дверей и направо, ориентир СМП Банк, далее прямо через сквер Космонавта Волкова, по дороге налево, выйти на ул. Космонавта Волкова,идти прямо, на светофоре перейти дорогу на противоположную сторону и, по ходу движения автомобилей, пройти одно здание (10 минут).</p>

                                            <h5>На метро и общественном транспорте:</h5>
                                            <p>м. Войковская, последний вагон из Центра, выход в город направо в сторону платформы электричек, троллейбус № 57, остановка «улица Космонавта Волкова», пройти немного назад, против движения автомобилей (не более 5 минут).</p>

                                            <h5>На автомобиле:</h5>
                                            <p>Из центра по Ленинградскому ш. перед м. Войковская, на мосту Победы съезд вправо, на светофоре налево, по ул. Космонавта Волкова до первого светофора. За светофором третье здание справа (не более 3 минут).</p>

                                            <p><strong>Координаты (для навигатора)</strong>: N 55.815483, E 37.515811</p>
                                            <p>На нашем сайте работает интернет-магазин мебели. С его помощью Вам не понадобится тратить свое драгоценное время для походов по магазинам в поиске подходящей мебели. Интернет-магазин мебели работает круглосуточно без праздников и выходных, заявки обрабатываются в течение рабочего дня. Вы можете самостоятельно оформить заказ на понравившуюся модель мебели в любое удобное время.</p>

                                        </div>
                                    </div>
                                    <div class="contact-right">
                                        <div class="heading-gray">
                                            <div class="border-wrap">
                                                <h4>Написать руководителю</h4>
                                            </div>
                                        </div>
                                        <div class="contact-wrap">
                                            <form class="form">
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-6 col-lg-12">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="name" placeholder="Имя">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-12">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="email" placeholder="Email">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <textarea class="form-control" name="message" placeholder="Сообщение" rows="6"></textarea>
                                                </div>
                                                <div class="form-btn">
                                                    <button type="submit" class="btn btn-orange">Отправить</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="contact-phone-block">
                            <div class="container">
                                <div class="heading-gray">
                                    <div class="border-wrap">
                                        <h4>Куда звонить</h4>
                                    </div>
                                </div>
                                <ul class="contact-phone">
                                    <li>
                                        <div class="contact-title">Телефоны:</div>
                                        <p>
                                            +7 (495) 661-71-58
                                            <small>многоканальный</small>
                                            <br/>
                                            <a class="tel" href="tel:+74957788319">+7 (495) 77-88-319</a>
                                            <br/>
                                            <a class="tel" href="tel:+79672255180">+7 (967) 22-55-180</a>
                                            <br/>
                                            <a class="tel" href="tel:+79672255380">+7 (495) 77-88-319</a>
                                        </p>
                                    </li>
                                    <li>
                                        <div class="contact-title">Факс:</div>
                                        <p>+7 (495) 77-88-319</p>
                                        <div class="contact-title">E-mail:</div>
                                        <a href="mailto:info@dcd.ru">info@dcd.ru</a>
                                        <br/>
                                        <a href="#" class="btn-modal">Заказать звонок</a>
                                    </li>
                                    <li></li>
                                    <li></li>
                                </ul>
                            </div>

                        </div>

                        <div class="contact-staff-block">
                            <div class="container">
                                <ul class="contact-staff">
                                    <li>
                                        <div class="staff-box">
                                            <div class="contact-title">Наши сотрудники</div>
                                            <p>Задать любой вопрос по интересующей Вас продукции, а также отправить заявку на подбор, расчёт и согласование, выбранной Вами серии офисной мебели возможно, не снимая телефонной трубки, связавшись с нашими менеджерами по электронной почте или ICQ.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="staff-box">
                                            <div class="contact-title">Дмитрий Валерьевич</div>
                                            <p>Руководитель отдела нестандартных изделий</p>
                                            <p>Телефон: +7 (495) 661-71-58 добавочный 14</p>
                                            <p>ICQ: 411627011</p>
                                            <p>Эл.почта: <a href="mailto:info@dcd.ru">info@dcd.ru</a></p>
                                        </div>
                                        <div class="staff-box">
                                            <div class="contact-title">Василий</div>
                                            <p>Корпоративные и оптовые продажи, проектирование</p>
                                            <p>Телефон: +7 (495) 661-71-58 добавочный 17</p>
                                            <p>ICQ: 235972447</p>
                                            <p>Эл.почта: <a href="mailto:tvs@dcd.ru">tvs@dcd.ru</a></p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="staff-box">
                                            <div class="contact-title">Сергей Анатольевич</div>
                                            <p>Руководитель тендерного отдела</p>
                                            <p>Телефон: +7 (495) 661-71-58 добавочный 13</p>
                                            <p>ICQ: 212851598</p>
                                            <p>Эл.почта: <a href="mailto:opt@dcd.ru">opt@dcd.ru</a></p>
                                        </div>
                                        <div class="staff-box">
                                            <div class="contact-title">Елена</div>
                                            <p>Оформление документов, бухгалтерия</p>
                                            <p>Телефон: +7 (495) 661-71-58 добавочный 11</p>
                                            <p>ICQ: 604752127</p>
                                            <p>Эл.почта: <a href="mailto:helena@dcd.ruv">helena@dcd.ruv</a>
                                        </div>

                                    </li>
                                    <li>
                                        <div class="staff-box">
                                            <div class="contact-title">Алексей</div>
                                            <p>Розничные и оптовые продажи</p>
                                            <p> Телефон: +7 (495) 661-71-58 добавочный 15</p>
                                            <p>ICQ: 416770836</p>
                                            <p>Эл.почта: <a href="mailto:alex@dcd.ru">alex@dcd.ru</a></p>
                                        </div>
                                        <div class="staff-box">
                                            <div class="contact-title">Екатерина</div>
                                            <p>Администратор сайта</p>
                                            <p>Телефон: +7 (495) 661-71-58 добавочный 16</p>
                                            <p>Эл.почта: <a href="mailto:tokarevaem@yandex.ru">tokarevaem@yandex.ru</a></p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>


                </section>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- script -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

        <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

    <script>
        var myMap;

        ymaps.ready(function () {
            myMap = new ymaps.Map('map', {
                zoom: 17,
                center: [55.81541256892136,37.514804999999974],
                controls: ['smallMapDefaultSet']
            }, {
                searchControlProvider: 'yandex#search'
            });
            myMap.geoObjects
                .add(new ymaps.Placemark([55.81541256892136,37.514804999999974], {
                    balloonContent: ''
                }, {
                    preset: 'islands#icon',
                    iconColor: '#99afd'
                }));
        });
    </script>

    </body>
</html>
