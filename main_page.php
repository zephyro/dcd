<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Контакты</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Заголовок</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->


                    <!-- Main content -->
                    <div class="content">
                        <div class="container">
                            <ul class="filter-list">
                                <li>
                                    <label class="radio">
                                        <input class="radio-input" type="radio" name="type" value="Мебель для руководителя">
                                        <span class="radio-text">Мебель для руководителя</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="radio">
                                        <input class="radio-input" type="radio" name="type" value="Мебель для персонала">
                                        <span class="radio-text">Мебель для персонала</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="radio">
                                        <input class="radio-input" type="radio" name="type" value="Мебель для домы">
                                        <span class="radio-text">Мебель для домы</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="radio">
                                        <input class="radio-input" type="radio" name="type" value="Мебель для ресепшн">
                                        <span class="radio-text">Мебель для ресепшн</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="radio">
                                        <input class="radio-input" type="radio" name="type" value="Мебель для кухни">
                                        <span class="radio-text">Мебель для кухни</span>
                                    </label>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <!-- -->

                </section>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- script -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
