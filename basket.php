<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>
        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- return  -->
                                <a class="heading-return" href="#"><i class="fa fa-angle-left"></i> <span>Вернуться в магазин</span></a>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Корзина</h1>
                                <!-- -->

                                <ul class="heading-action">
                                    <li>
                                        <a href="#">
                                            <svg class="ico-svg" viewBox="0 0 26 27" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-print" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <svg class="ico-svg" viewBox="0 0 20 27" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-pdf" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <svg class="ico-svg" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-save" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <!-- -->


                    <!-- Main content -->
                    <div class="content content-overflow">
                        <div class="container">

                            <div class="basket">

                                <div class="basket-item">
                                    <ul class="basket-row">
                                        <li>
                                            <a href="#" class="basket-image">
                                                <img src="images/product_backet_02.jpg" class="img-responsive" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <div class="basket-product"><a href="#">Стул Cappuccino chrome</a></div>
                                            <a href="#" class="basket-product-color">
                                                <span class="product-color-type"><img src="img/color_brown.jpg" alt=""></span>
                                                <span class="product-color-name">Кожа орех / коричневая</span>
                                            </a>
                                        </li>
                                        <li>
                                            <label class="product-assembly">
                                                <input class="checkbox" type="checkbox" name="assembly" checked>
                                                <span class="checkbox-icon"></span>
                                                <span class="checkbox-label">
                                            <svg class="ico-svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-assembly" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                            <span>Сборка (1 800 a)</span>
                                        </span>
                                            </label>
                                        </li>
                                        <li>
                                            <div class="form-number">
                                                <span class="minus">-</span>
                                                <span class="plus">+</span>
                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                            </div>
                                            <div class="basket-price">
                                                <div class="basket-price-inner">
                                                    <span class="basket-price-new">5 298 <i class="fa fa-ruble"></i></span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <span class="basket-delete"></span>
                                </div>

                                <div class="basket-item">
                                    <ul class="basket-row">
                                        <li>
                                            <a href="#" class="basket-image">
                                                <img src="images/product_backet_01.jpg" class="img-responsive" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <div class="basket-product"><a href="#">Стул Cappuccino chrome</a></div>
                                            <a href="#" class="basket-product-color">
                                                <span class="product-color-type"><img src="img/color_brown.jpg" alt=""></span>
                                                <span class="product-color-name">Кожа орех / коричневая</span>
                                            </a>
                                        </li>
                                        <li>
                                            <label class="product-assembly">
                                                <input class="checkbox" type="checkbox" name="assembly" checked>
                                                <span class="checkbox-icon"></span>
                                                <span class="checkbox-label">
                                            <svg class="ico-svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-assembly" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                            <span>Сборка (1 800 a)</span>
                                        </span>
                                            </label>
                                        </li>
                                        <li>
                                            <div class="form-number">
                                                <span class="minus">-</span>
                                                <span class="plus">+</span>
                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                            </div>
                                            <div class="basket-price">
                                                <div class="basket-price-inner">
                                                    <span class="basket-price-source">3 209 <i class="fa fa-ruble"></i> c х 2 </span>
                                                    <span class="basket-price-new">5 298 <i class="fa fa-ruble"></i></span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <span class="basket-delete"></span>
                                </div>

                                <div class="basket-item">
                                    <ul class="basket-row">
                                        <li>
                                            <a href="#" class="basket-image">
                                                <img src="images/product_backet_01.jpg" class="img-responsive" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <div class="basket-product"><a href="#">Стул Cappuccino chrome</a></div>
                                            <a href="#" class="basket-product-color">
                                                <span class="product-color-type"><img src="img/color_brown.jpg" alt=""></span>
                                                <span class="product-color-name">Кожа орех / коричневая</span>
                                            </a>
                                        </li>
                                        <li>
                                            <label class="product-assembly">
                                                <input class="checkbox" type="checkbox" name="assembly" checked>
                                                <span class="checkbox-icon"></span>
                                                <span class="checkbox-label">
                                            <svg class="ico-svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-assembly" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                            <span>Сборка (1 800 a)</span>
                                        </span>
                                            </label>
                                        </li>
                                        <li>
                                            <div class="form-number">
                                                <span class="minus">-</span>
                                                <span class="plus">+</span>
                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                            </div>
                                            <div class="basket-price">
                                                <div class="basket-price-inner">
                                                    <span class="basket-price-source">3 209 <i class="fa fa-ruble"></i> c х 2 </span>
                                                    <span class="basket-price-new">5 298 <i class="fa fa-ruble"></i></span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <span class="basket-delete"></span>
                                </div>

                            </div>

                            <ul class="basket-footer">

                                <li class="row-promo">
                                    <div class="basket-promo">
                                        <label class="basket-promo-label">Введите</label>
                                        <div class="basket-promo-form">
                                            <input type="text" class="basket-promo-input" name="" placeholder="Промо-код">
                                            <button type="submit" class="basket-promo-btn">Подтвердить</button>
                                        </div>
                                    </div>

                                </li>

                                <li class="row-price">
                                    <div class="clearfix">
                                        <ul class="basket-total-value">
                                            <li>Товаров: 4</li>
                                            <li>Вес заказа: 50,3кг</li>
                                            <li>Объем заказа: 5,1м3</li>
                                        </ul>

                                        <ul class="basket-footer-price">
                                            <li>Цена:</li>
                                            <li>16 614 <i class="fa fa-ruble"></i></li>
                                        </ul>
                                    </div>

                                </li>

                                <li class="row-discount basket-footer-gray">
                                    <ul class="basket-discount">
                                        <li>
                                            <div class="basket-discount-label">
                                                <span class="discount-icon"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                                                <span>Скидка 5%:</span>

                                            </div>
                                            <p>Докупите до <strong>20 000</strong> <i class="fa fa-ruble"></i></p>
                                            <p>и увеличьте скидку до <strong>10%</strong></p>
                                        </li>
                                        <li>
                                            <span class="basket-discount-value">-830 <i class="fa fa-ruble"></i></span>
                                        </li>
                                    </ul>
                                </li>

                                <li class="row-summary">
                                    <ul class="basket-summary">
                                        <li>Итого:</li>
                                        <li>15 784 <i class="fa fa-ruble"></i></li>
                                    </ul>
                                </li>
                                <li>
                                    <button type="submit" class="btn btn-orange">Оформить заказ</button>
                                </li>

                            </ul>

                        </div>
                    </div>
                    <!-- -->

                </section>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
