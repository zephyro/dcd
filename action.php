<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

    <div class="page">
        <div class="page-inner">
            <!-- Header -->
            <?php include('inc/header.inc.php') ?>
            <!-- -->

            <!-- Navigation -->
            <?php include('inc/navbar.inc.php') ?>
            <!-- -->

            <!-- Main page -->
            <section class="main">

                <!-- Main heading -->
                <div class="heading">
                    <div class="container">
                        <div class="border-wrap">

                            <!-- Breadcrumbs -->
                            <ul class="breadcrumbs">
                                <li><a href="#">Главная</a></li>
                                <li>Спецпредложения</li>
                            </ul>
                            <!-- -->

                            <!-- Heading -->
                            <h1>Спецпредложения</h1>
                            <!-- -->

                        </div>
                    </div>
                </div>
                <!-- -->

                <!-- Nav heading -->
                <nav class="heading-nav">
                    <div class="container">
                        <ul>
                            <li class="active" ><a href="#">Скидки и акции</a></li>
                            <li><a href="#">Получить скидку</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- -->

                <!-- Slider -->
                <?php include('inc/jumbotron.inc.php') ?>
                <!-- -->

                <div class="sort sort-white">
                    <div class="container">
                        <div class="border-wrap">
                            <div class="clearfix">
                                <div class="sort-direction">
                                    <div class="clearfix">
                                        <span class="sort-legend">Сортировать:</span>
                                        <ul class="sort-elem">
                                            <li>По цене</li>
                                            <li>
                                                <label class="sort-item sort-item-down">
                                                    <input type="radio" class="sort-radio" name="price">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-down"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                                <label class="sort-item sort-item-up">
                                                    <input type="radio" class="sort-radio" name="price">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-up"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                        <ul class="sort-elem">
                                            <li>По популярности</li>
                                            <li>
                                                <label class="sort-item sort-item-down">
                                                    <input type="radio" class="sort-radio" name="pop">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-down"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                                <label class="sort-item sort-item-up">
                                                    <input type="radio" class="sort-radio" name="pop">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7" name="pop">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-up"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <ul class="sort-view">
                                    <li>Показать:</li>
                                    <li>
                                        <div class="view-box select-nav">
                                            <div class="select-nav-bg"></div>
                                            <div class="select-nav-wrap">
                                                <a class="view-box-current select-nav-current" href="#"><span>Все товары</span></a>
                                                <ul class="view-list select-nav-dropdown">
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views" class="custom-radio" value="Столы">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 35 33">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-stol"></use>
                                                                </svg>
                                                                <span>Столы</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views" class="custom-radio" value="мягкие Кресла">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 32 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-mjagkie-kresla"></use>
                                                                </svg>
                                                                <span>мягкие Кресла</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="стулья и кресла">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 20 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-stulja-kresla"></use>
                                                                </svg>
                                                                <span>стулья и кресла</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="Диваны">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 41 26">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-divany"></use>
                                                                </svg>
                                                                <span>Диваны</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="Шкафы">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 24 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-shkaf"></use>
                                                                </svg>
                                                                <span>Шкафы</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="Тумбы">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 27 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-tumba"></use>
                                                                </svg>
                                                                <span>Тумбы</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="Стеллажи">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 21 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-stellag"></use>
                                                                </svg>
                                                                <span>Стеллажи</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="Столы переговорные">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 35 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-peregovornaja"></use>
                                                                </svg>
                                                                <span>Столы переговорные</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views" class="custom-radio" value="Столы переговорные">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 31 32">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-all"></use>
                                                                </svg>
                                                                <span>Все товары</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Main content -->
                <div class="content content-overflow">

                    <div class="container box-border-top">
                        <div class="border-wrap">
                            <ul class="product-tile">

                                <li>
                                    <div class="showcase">
                                        <form class="form">

                                            <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                            </div>

                                            <a href="#" class="showcase-image">
                                                <img src="images/showcase_product_01.jpg" class="img-responsive" alt="">
                                            </a>
                                            <div class="showcase-content">
                                                <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                <div class="colors">
                                                    <ul class="colors-list">
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li class="last">
                                                            <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="showcase-purchase">
                                                    <li>
                                                        <div class="form-number">
                                                            <span class="minus">-</span>
                                                            <span class="plus">+</span>
                                                            <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                        <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>

                                                <button type="submit" class="btn">В корзину</button>
                                            </div>
                                        </form>
                                    </div>
                                </li>

                                <li>
                                    <div class="showcase">
                                        <form class="form">

                                            <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                            </div>

                                            <a href="#" class="showcase-image">
                                                <img src="images/showcase_product_02.jpg" class="img-responsive" alt="">
                                            </a>
                                            <div class="showcase-content">
                                                <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                <div class="colors">
                                                    <ul class="colors-list">
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_06.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_07.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="showcase-purchase">
                                                    <li>
                                                        <div class="form-number">
                                                            <span class="minus">-</span>
                                                            <span class="plus">+</span>
                                                            <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>

                                                <button type="submit" class="btn">В корзину</button>
                                            </div>
                                        </form>
                                    </div>
                                </li>

                                <li>
                                    <div class="showcase">
                                        <form class="form">

                                            <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                            </div>

                                            <a href="#" class="showcase-image">
                                                <img src="images/showcase_product_03.jpg" class="img-responsive" alt="">
                                            </a>
                                            <div class="showcase-content">
                                                <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                <div class="colors colors-selected">
                                                    <ul class="colors-list">
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li class="last">
                                                            <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                        </li>
                                                    </ul>

                                                    <ul class="colors-current">
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio" checked disabled>
                                                                <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                        </li>
                                                        <li>
                                                            <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="showcase-purchase">
                                                    <li>
                                                        <div class="form-number">
                                                            <span class="minus">-</span>
                                                            <span class="plus">+</span>
                                                            <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                        <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>

                                                <button type="submit" class="btn">В корзину</button>
                                            </div>
                                        </form>
                                    </div>
                                </li>

                                <li>
                                    <div class="showcase">
                                        <form class="form">

                                            <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                            </div>

                                            <a href="#" class="showcase-image">
                                                <img src="images/showcase_product_04.jpg" class="img-responsive" alt="">
                                            </a>
                                            <div class="showcase-content">
                                                <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                <div class="colors">
                                                    <ul class="colors-list">
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_06.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_07.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="showcase-purchase">
                                                    <li>
                                                        <div class="form-number">
                                                            <span class="minus">-</span>
                                                            <span class="plus">+</span>
                                                            <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>

                                                <button type="submit" class="btn">В корзину</button>
                                            </div>
                                        </form>
                                    </div>
                                </li>

                                <li>
                                    <div class="showcase">
                                        <form class="form">

                                            <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                            </div>

                                            <a href="#" class="showcase-image">
                                                <img src="images/showcase_product_01.jpg" class="img-responsive" alt="">
                                            </a>
                                            <div class="showcase-content">
                                                <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                <div class="colors">
                                                    <ul class="colors-list">
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li class="last">
                                                            <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="showcase-purchase">
                                                    <li>
                                                        <div class="form-number">
                                                            <span class="minus">-</span>
                                                            <span class="plus">+</span>
                                                            <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                        <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>

                                                <button type="submit" class="btn">В корзину</button>
                                            </div>
                                        </form>
                                    </div>
                                </li>

                                <li>
                                    <div class="showcase">
                                        <form class="form">

                                            <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                            </div>

                                            <a href="#" class="showcase-image">
                                                <img src="images/showcase_product_02.jpg" class="img-responsive" alt="">
                                            </a>
                                            <div class="showcase-content">
                                                <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                <div class="colors">
                                                    <ul class="colors-list">
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_06.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_07.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="showcase-purchase">
                                                    <li>
                                                        <div class="form-number">
                                                            <span class="minus">-</span>
                                                            <span class="plus">+</span>
                                                            <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>

                                                <button type="submit" class="btn">В корзину</button>
                                            </div>
                                        </form>
                                    </div>
                                </li>

                                <li>
                                    <div class="showcase">
                                        <form class="form">

                                            <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                            </div>

                                            <a href="#" class="showcase-image">
                                                <img src="images/showcase_product_03.jpg" class="img-responsive" alt="">
                                            </a>
                                            <div class="showcase-content">
                                                <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                <div class="colors">
                                                    <ul class="colors-list">
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li class="last">
                                                            <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="showcase-purchase">
                                                    <li>
                                                        <div class="form-number">
                                                            <span class="minus">-</span>
                                                            <span class="plus">+</span>
                                                            <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                        <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>

                                                <button type="submit" class="btn">В корзину</button>
                                            </div>
                                        </form>
                                    </div>
                                </li>

                                <li>
                                    <div class="showcase">
                                        <form class="form">

                                            <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                            </div>

                                            <a href="#" class="showcase-image">
                                                <img src="images/showcase_product_04.jpg" class="img-responsive" alt="">
                                            </a>
                                            <div class="showcase-content">
                                                <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                <div class="colors">
                                                    <ul class="colors-list">
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_06.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_07.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                        <li>
                                                            <label>
                                                                <input type="radio" name="radio">
                                                                <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="showcase-purchase">
                                                    <li>
                                                        <div class="form-number">
                                                            <span class="minus">-</span>
                                                            <span class="plus">+</span>
                                                            <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                    </li>
                                                </ul>

                                                <button type="submit" class="btn">В корзину</button>
                                            </div>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>

            </section>
            <!-- -->
        </div>

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

    </div>



        <!-- Scripts -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
