<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <div class="navbar-label"></div>

                <nav class="navbar">
                    <div class="container">
                        <div class="navbar-inner">
                            <div class="navbar-wrap">
                                <span class="navbar-close"></span>
                                <div class="navbar-search">
                                    <form class="form">
                                        <input type="text" name="search" class="form-search" placeholder="Поиск товаров">
                                        <button type="submit" class="btn-search">
                                            <svg class="ico-svg" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite-icons.svg#icon-loop" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </button>
                                    </form>
                                </div>
                                <div class="navbar-delete">
                                    <div class="navbar-delete-inner"></div>
                                </div>
                                <ul class="topnav">
                                    <li class="dropdown">
                                        <a href="#">каталог <i class="fa fa-chevron-down"></i></a>
                                        <div class="dropnav dropnav-cat">
                                            <ul>
                                                <li class="dropdown">
                                                    <a href="#"><span>мебель для руководителя</span> <i class="fa fa-chevron-down"></i></a>
                                                    <ul class="dropnav-second">
                                                        <li><a href="#">Бюджет – от 4 150 p</a></li>
                                                        <li><a href="#">Эконом – от 7 367 p</a></li>
                                                        <li><a href="#">Бизнес – от 27 111 p</a></li>
                                                        <li><a href="#">Премиум – от 53 130 p</a></li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#"><span>мебель для персонала</span> <i class="fa fa-chevron-down"></i></a>
                                                    <ul class="dropnav-second">
                                                        <li><a href="#">Бюджет – от 4 150 p</a></li>
                                                        <li><a href="#">Эконом – от 7 367 p</a></li>
                                                        <li><a href="#">Бизнес – от 27 111 p</a></li>
                                                        <li><a href="#">Премиум – от 53 130 p</a></li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#"><span>ресепшн</span> <i class="fa fa-chevron-down"></i></a>
                                                    <ul class="dropnav-second">
                                                        <li><a href="#">Бюджет – от 4 150 p</a></li>
                                                        <li><a href="#">Эконом – от 7 367 p</a></li>
                                                        <li><a href="#">Бизнес – от 27 111 p</a></li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#"><span>мебель специальная</span> <i class="fa fa-chevron-down"></i></a>
                                                    <ul class="dropnav-second">
                                                        <li><a href="#">Офисные перегородки</a></li>
                                                        <li><a href="#">Журнальные обеденные столы</a></li>
                                                        <li><a href="#">Металлическая мебель, сейфы</a></li>

                                                    </ul>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#"><span>сопутствующие товары</span> <i class="fa fa-chevron-down"></i></a>
                                                    <ul class="dropnav-second">
                                                        <li><a href="#">Светильники</a></li>
                                                        <li><a href="#">Часы</a></li>
                                                        <li><a href="#">Вешалки, зеркала</a></li>
                                                        <li><a href="#">Коврики и накладки</a></li>
                                                        <li><a href="#">Растения</a></li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#"><span>стулья и кресла</span> <i class="fa fa-chevron-down"></i></a>
                                                    <ul class="dropnav-second">
                                                        <li><a href="#">Кресла для руководителей</a></li>
                                                        <li><a href="#">Кресла для персонала</a></li>
                                                        <li><a href="#">Стулья</a></li>
                                                        <li><a href="#">Стулья для кафе</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li class="dropdown">
                                                    <a href="#"><span>медицинская мебель</span> <i class="fa fa-chevron-down"></i></a>
                                                    <ul class="dropnav-second">
                                                        <li><a href="#">Лабораторная мебель</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#"><span>мини кухни и шкафы</span></a></li>
                                                <li><a href="#"><span>мебель для компьютера</span></a></li>
                                                <li><a href="#"><span>мягкая офисная мебель</span></a></li>
                                                <li><a href="#"><span>мебель для гостиниц</span></a></li>
                                                <li><a href="#"><span>мебель для дома</span></a>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li><a href="#"><span>мебель на заказ</span></a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="#"><span>школьная мебель</span> <i class="fa fa-chevron-down"></i></a>
                                                    <ul class="dropnav-second">
                                                        <li><a href="#">Мебель дял аудиторий</a></li>
                                                    </ul>
                                                </li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                                <li></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li><a href="#">О компании</a></li>
                                    <li><a href="#">Услуги</a></li>
                                    <li><a href="#">Спецпредложения</a></li>
                                    <li><a href="#">Оплата</a></li>
                                    <li><a href="#">Контакты</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>

                <!-- Filter button -->
                <a href="#" class="filter-btn filter-btn-inside open">
                    <svg class="ico-svg" viewBox="0 0 33 33" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite-icons.svg#icon-selection" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                    <span>подбор мебели</span>
                </a>
                <!-- -->

                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Мебель для руководителей</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <div class="heading-wrap">
                                    <h1>Мебель для руководителей</h1>
                                    <ul class="heading-product-cat">
                                        <li><a href="#budget" class="btn-scroll">Бюджет</a></li>
                                        <li><a href="#economy" class="btn-scroll">Эконом</a></li>
                                        <li><a href="#business" class="btn-scroll">Бизнес</a></li>
                                        <li><a href="#premium" class="btn-scroll">Премиум</a></li>
                                    </ul>
                                </div>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <div class="sort sort-white">
                        <div class="container">
                            <div class="border-wrap">
                                <div class="clearfix">
                                    <div class="sort-group-left">
                                        <span class="sort-legend">Сортировать:</span>
                                        <ul class="sort-elem">
                                            <li>По цене</li>
                                            <li>
                                                <label class="sort-item sort-item-down">
                                                    <input type="radio" class="sort-radio" name="price">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-down"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                                <label class="sort-item sort-item-up">
                                                    <input type="radio" class="sort-radio" name="price">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-up"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                        <ul class="sort-elem">
                                            <li>По популярности</li>
                                            <li>
                                                <label class="sort-item sort-item-down">
                                                    <input type="radio" class="sort-radio" name="pop">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-down"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                                <label class="sort-item sort-item-up">
                                                    <input type="radio" class="sort-radio" name="pop">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7" name="pop">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-up"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Main content -->
                    <div class="content content-overflow">
                        <div class="container">
                            <div class="border-wrap">
                                <div class="rows open">

                                    <div class="col-sidebar">
                                        <div class="filter-content">

                                            <div class="filter-header">
                                                <div class="filter-header-wrap">
                                                    <div class="filter-header-text">подбор мебели</div>
                                                    <a class="filter-reset" href="#"><span>Сбросить фильтр</span></a>
                                                </div>
                                                <svg class="ico-svg" viewBox="0 0 33 33" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite-icons.svg#icon-selection" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                                <span class="sidebar-filter-close"></span>
                                            </div>

                                            <div class="filter-main">

                                                <div class="filter-group filter-group-list">
                                                    <div class="filter-group-header">
                                                        <span class="filter-group-reset"></span>
                                                        <div class="filter-group-name">Категория мебели</div>
                                                        <div class="filter-group-value">Мебель для руководителя <span class="slide-button"></span></div>
                                                    </div>
                                                    <div class="filter-group-content">
                                                        <ul class="filter-list">
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="type" value="Мебель для руководителя">
                                                                    <span class="radio-text">Мебель для руководителя</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="type" value="Мебель для персонала">
                                                                    <span class="radio-text">Мебель для персонала</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="type" value="Мебель для домы">
                                                                    <span class="radio-text">Мебель для домы</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="type" value="Мебель для ресепшн">
                                                                    <span class="radio-text">Мебель для ресепшн</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="type" value="Мебель для кухни">
                                                                    <span class="radio-text">Мебель для кухни</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="filter-group filter-group-list">
                                                    <div class="filter-group-header">
                                                        <span class="filter-group-reset"></span>
                                                        <div class="filter-group-name">Ценовая группа</div>
                                                        <div class="filter-group-value">Все группы <span class="slide-button"></span></div>
                                                    </div>
                                                    <div class="filter-group-content">
                                                        <ul class="filter-list">
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="status" value="Все группы">
                                                                    <span class="radio-text">Все серии</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="status" value="Бюджет">
                                                                    <span class="radio-text">Бюджет</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="status" value="Эконом">
                                                                    <span class="radio-text">Эконом</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="status" value="Стандарт">
                                                                    <span class="radio-text">Стандарт</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="status" value="Премиум">
                                                                    <span class="radio-text">Премиум</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="filter-group filter-group-list">
                                                    <div class="filter-group-header">
                                                        <span class="filter-group-reset"></span>
                                                        <div class="filter-group-name">Серия</div>
                                                        <div class="filter-group-value">Все серии <span class="slide-button"></span></div>
                                                    </div>
                                                    <div class="filter-group-content">
                                                        <ul class="filter-list">
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Все серии" checked>
                                                                    <span class="radio-text">Все серии</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Приоритет">
                                                                    <span class="radio-text">Приоритет</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Born (Борн)">
                                                                    <span class="radio-text">Born (Борн)</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Vasanta (Васанта)">
                                                                    <span class="radio-text">Vasanta (Васанта)</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Дипломат">
                                                                    <span class="radio-text">Дипломат</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Полонез">
                                                                    <span class="radio-text">Полонез</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Марс">
                                                                    <span class="radio-text">Марс</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Марс Люкс">
                                                                    <span class="radio-text">Марс Люкс</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Boston director">
                                                                    <span class="radio-text">Boston director</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Бостон">
                                                                    <span class="radio-text">(Бостон)</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Патриот">
                                                                    <span class="radio-text">Патриот</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Престиж">
                                                                    <span class="radio-text">Престиж</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="series" value="Мастер">
                                                                    <span class="radio-text">Мастер</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="filter-group filter-group-list">
                                                    <div class="filter-group-header">
                                                        <span class="filter-group-reset"></span>
                                                        <div class="filter-group-name">Тип мебели</div>
                                                        <div class="filter-group-value">Все товары <span class="slide-button"></span></div>
                                                    </div>
                                                    <div class="filter-group-content">
                                                        <ul class="filter-list">
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="product-type" value="Стол">
                                                                    <span class="radio-text">Стол</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="product-type" value="Кресла и стулья">
                                                                    <span class="radio-text">Кресла и стулья</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="product-type" value="Диваны">
                                                                    <span class="radio-text">Диваны</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="radio">
                                                                    <input class="radio-input" type="radio" name="product-type" value="Ресепшн">
                                                                    <span class="radio-text">Ресепшн</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="filter-group filter-group-list">
                                                    <div class="filter-group-header">
                                                        <span class="filter-group-reset"></span>
                                                        <div class="filter-group-name">Страна производитель</div>
                                                        <div class="filter-group-value"><span class="filter-value-item">Все товары</span> <span class="slide-button"></span></div>
                                                    </div>
                                                    <div class="filter-group-content">
                                                        <ul class="filter-list">
                                                            <li>
                                                                <label class="checkbox">
                                                                    <input class="checkbox-input" type="checkbox" name="product-type" value="Россия">
                                                                    <span class="checkbox-text">Россия</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox">
                                                                    <input class="radio-input" type="checkbox" name="product-type" value="Италия">
                                                                    <span class="checkbox-text">Италия</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox">
                                                                    <input class="checkbox-input" type="checkbox" name="product-type" value="Германия">
                                                                    <span class="checkbox-text">Германия</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox">
                                                                    <input class="checkbox-input" type="checkbox" name="product-type" value="Китай">
                                                                    <span class="checkbox-text">Китай</span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label class="checkbox">
                                                                    <input class="checkbox-input" type="checkbox" name="product-type" value="Корея">
                                                                    <span class="checkbox-text">Корея</span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                        <div class="text-right">
                                                            <span class="filter-group-close">Свернуть <i class="fa fa-angle-up"></i></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="filter-group">
                                                    <div class="filter-body">
                                                        <div class="filter-group-name">Цена серии</div>
                                                        <div class="filter-range">
                                                            <input type="text" id="range-price" name="range-price" value="" />
                                                            <ul class="filter-range-legend">
                                                                <li>от <span class="range-price-from">3 587 </span> <i class="fa fa-ruble"></i></li>
                                                                <li>до <span class="range-price-to">45000</span> <i class="fa fa-ruble"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="filter-group">
                                                    <div class="filter-body">
                                                        <div class="filter-group-name">Цвет серии</div>
                                                        <a href="#modal-color" class="filter-group-colors btn-modal-color">
                                                            <span class="colors-value">любой</span>
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="filter-group">
                                                    <div class="filter-group-header">
                                                        <span class="filter-group-reset"></span>
                                                        <div class="filter-group-name">Размер мебели, мм</div>
                                                        <ul class="filter-size">
                                                            <li>
                                                                <label class="mini-label">Ширина</label>
                                                                <input type="text" class="form-control" name="one" placeholder="90">
                                                            </li>
                                                            <li>
                                                                <label class="mini-label">Высота</label>
                                                                <input type="text" class="form-control" name="two" placeholder="70">
                                                            </li>
                                                            <li>
                                                                <label class="mini-label">Глубина</label>
                                                                <input type="text" class="form-control" name="three" placeholder="120">
                                                            </li>
                                                        </ul>
                                                    </div>

                                                </div>

                                                <div class="filter-group">
                                                    <div class="filter-body">
                                                        <div class="filter-group-name">толщина столешницы</div>
                                                        <div class="filter-range filter-range-text">
                                                            <input type="text" id="range-thick" name="range-thick" value="" />
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>



                                            <div class="filter-footer">
                                                <span class="filter-footer-text">Найдено товаров: 56</span>
                                                <button type="submit" class="btn btn-orange btn-sm">Показать</button>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-content">

                                        <div class="heading-gray" id="budget">
                                            <h3>Бюджет</h3>
                                        </div>

                                        <ul class="showcase-float">
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_16.jpg" class="img-responsive" alt="">
                                                    <h5>Шкафы купе</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_17.jpg" class="img-responsive" alt="">
                                                    <h5>Гардеробные</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_18.jpg" class="img-responsive" alt="">
                                                    <h5>Стойки ресепшн</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_18.jpg" class="img-responsive" alt="">
                                                    <h5>Стойки ресепшн</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>

                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_16.jpg" class="img-responsive" alt="">
                                                    <h5>Шкафы купе</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_17.jpg" class="img-responsive" alt="">
                                                    <h5>Гардеробные</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_18.jpg" class="img-responsive" alt="">
                                                    <h5>Стойки ресепшн</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_18.jpg" class="img-responsive" alt="">
                                                    <h5>Стойки ресепшн</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>

                                        </ul>

                                        <div class="heading-gray" id="economy">
                                            <h3>Эконом</h3>
                                        </div>

                                        <ul class="showcase-float">
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_16.jpg" class="img-responsive" alt="">
                                                    <h5>Шкафы купе</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_17.jpg" class="img-responsive" alt="">
                                                    <h5>Гардеробные</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_18.jpg" class="img-responsive" alt="">
                                                    <h5>Стойки ресепшн</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_18.jpg" class="img-responsive" alt="">
                                                    <h5>Стойки ресепшн</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>

                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_16.jpg" class="img-responsive" alt="">
                                                    <h5>Шкафы купе</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_17.jpg" class="img-responsive" alt="">
                                                    <h5>Гардеробные</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_18.jpg" class="img-responsive" alt="">
                                                    <h5>Стойки ресепшн</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="showcase-item" href="#">
                                                    <img src="images/category_18.jpg" class="img-responsive" alt="">
                                                    <h5>Стойки ресепшн</h5>
                                                    <ul class="showcase-info">
                                                        <li>
                                                            <span>Цена:</span>
                                                            <strong>от 8 558 <i class="fa fa-ruble"></i></strong>
                                                        </li>
                                                        <li>
                                                            <span>Срок поставки:</span>
                                                            <strong>на складе</strong>
                                                        </li>
                                                    </ul>
                                                </a>
                                            </li>

                                        </ul>

                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- -->


                </section>
                <!-- -->

                <!-- Leaders block -->
                <?php include('inc/leaders.inc.php') ?>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- script -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
