<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Услуги</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Услуги</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->


                    <!-- Main content -->
                    <div class="content content-overflow">
                        <div class="container">
                            <div class="service-wrap">
                                <ul class="service">
                                    <li>
                                        <a href="#" class="service-item">
                                            <div class="service-icon">
                                                <svg class="ico-svg" viewBox="0 0 34 50" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite-icons.svg#icon-service-installation" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </div>
                                            <h4><span>Сборка</span></h4>
                                            <p>Экономьте свое время с нашими сборщиками</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="service-item">
                                            <div class="service-icon">
                                                <svg class="ico-svg" viewBox="0 0 56 50" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite-icons.svg#icon-service-warranty" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </div>
                                            <h4><span>Гарантия</span></h4>
                                            <p>На разные категории мебели 12-60 месяцев</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="service-item">
                                            <div class="service-icon">
                                                <svg class="ico-svg" viewBox="0 0 46 50" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite-icons.svg#icon-service-repair" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </div>
                                            <h4><span>Ремонт мебели</span></h4>
                                            <p>Предоставляем ремонт по гарантии и заказу</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="service-item">
                                            <div class="service-icon">
                                                <svg class="ico-svg" viewBox="0 0 63 47" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite-icons.svg#icon-service-shipping" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </div>
                                            <h4><span>Доставка</span></h4>
                                            <p>На разные категории мебели 12-60 месяцев</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="service-item">
                                            <div class="service-icon">
                                                <svg class="ico-svg" viewBox="0 0 37 50" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite-icons.svg#icon-service-design" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </div>
                                            <h4><span>Дизайн-проектирование</span></h4>
                                            <p>При покупке мебели у нас - бесплатно</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="service-item">
                                            <div class="service-icon">
                                                <svg class="ico-svg" viewBox="0 0 57 52" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite-icons.svg#icon-service-moving" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </div>
                                            <h4><span>офисный переезд</span></h4>
                                            <p>Поможем загрузить и перевезти мебель</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- -->

                </section>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->


        </div>

        <!-- Scripts -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
