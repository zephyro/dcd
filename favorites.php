<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Избранное</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Избранное</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <!-- Main content -->
                    <div class="content content-overflow">

                        <div class="container">
                            <div class="border-wrap">

                                <ul class="product-tile">

                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <div class="icon-status">
                                            <span class="icon-action">
                                                <svg class="ico-svg" viewBox="0 0 13 20">
                                                    <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                                </svg>
                                            </span>
                                                </div>

                                                <span class="showcase-delete"></span>

                                                <a href="#" class="showcase-image">
                                                    <img src="images/showcase_product_01.jpg" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#" class="btn-more-colors">Еще цвета</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                                </div>

                                                <span class="showcase-delete"></span>

                                                <a href="#" class="showcase-image">
                                                    <img src="images/showcase_product_02.jpg" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_06.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_07.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                                </div>

                                                <span class="showcase-delete"></span>

                                                <a href="#" class="showcase-image">
                                                    <img src="images/showcase_product_03.jpg" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#" class="btn-more-colors">Еще цвета</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                                </div>

                                                <span class="showcase-delete"></span>

                                                <a href="#" class="showcase-image">
                                                    <img src="images/showcase_product_04.jpg" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_06.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_07.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <div class="icon-status">
                                            <span class="icon-action">
                                                <svg class="ico-svg" viewBox="0 0 13 20">
                                                    <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                                </svg>
                                            </span>
                                                </div>

                                                <span class="showcase-delete"></span>

                                                <a href="#" class="showcase-image">
                                                    <img src="images/showcase_product_01.jpg" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#" class="btn-more-colors">Еще цвета</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                                </div>

                                                <span class="showcase-delete"></span>

                                                <a href="#" class="showcase-image">
                                                    <img src="images/showcase_product_02.jpg" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_06.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_07.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                                </div>

                                                <span class="showcase-delete"></span>

                                                <a href="#" class="showcase-image">
                                                    <img src="images/showcase_product_03.jpg" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#" class="btn-more-colors">Еще цвета</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <div class="icon-status">
                                        <span class="icon-action">
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                        </span>
                                                </div>

                                                <span class="showcase-delete"></span>

                                                <a href="#" class="showcase-image">
                                                    <img src="images/showcase_product_04.jpg" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_06.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_07.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- -->
                </section>
                <!-- -->

                <!-- Leaders block -->
                <?php include('inc/leaders.inc.php') ?>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
