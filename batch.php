<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li><a href="#">Мебель для руководителей</a></li>
                                    <li>Приоритет</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Приоритет</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <!-- Main content -->
                    <div class="content">

                        <div class="container">
                            <div class="border-wrap">

                                <div class="series">

                                    <div class="series-gallery">

                                        <div class="image-large">
                                            <a href="images/series_01.jpg" class="image-large-item" data-fancybox="gallery">
                                                <img src="images/series_01.jpg" class="img-responsive" alt="">
                                            </a>
                                            <a href="images/series_02.jpg" class="image-large-item" data-fancybox="gallery">
                                                <img src="images/series_02.jpg" class="img-responsive" alt="">
                                            </a>
                                            <a href="images/series_03.jpg" class="image-large-item" data-fancybox="gallery">
                                                <img src="images/series_03.jpg" class="img-responsive" alt="">
                                            </a>
                                            <a href="images/series_04.jpg" class="image-large-item" data-fancybox="gallery">
                                                <img src="images/series_04.jpg" class="img-responsive" alt="">
                                            </a>
                                            <a href="images/series_05.jpg" class="image-large-item" data-fancybox="gallery">
                                                <img src="images/series_05.jpg" class="img-responsive" alt="">
                                            </a>
                                        </div>

                                        <div class="image-thumb">
                                            <div class="image-thumb-slider">
                                                <div class="image-thumb-item">
                                                    <img src="images/series_01.jpg" class="img-responsive" alt="">
                                                </div>
                                                <div class="image-thumb-item">
                                                    <img src="images/series_02.jpg" class="img-responsive" alt="">
                                                </div>
                                                <div class="image-thumb-item">
                                                    <img src="images/series_03.jpg" class="img-responsive" alt="">
                                                </div>
                                                <div class="image-thumb-item">
                                                    <img src="images/series_04.jpg" class="img-responsive" alt="">
                                                </div>
                                                <div class="image-thumb-item">
                                                    <img src="images/series_05.jpg" class="img-responsive" alt="">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="series-content">
                                        <div class="series-tabs">

                                            <ul class="series-tabs-nav">
                                                <li class="nav1 active"><a href="#" data-target=".tab1">Минимальный комплект</a></li>
                                                <li class="nav2"><a href="#" data-target=".tab2">Описание</a></li>
                                                <li class="nav3"><a href="#" data-target=".tab3">Характеристики</a></li>
                                            </ul>

                                            <div class="series-tabs-item tab1 active">
                                                <div class="tabs-header" data-target=".nav1">Минимальный комплект</div>
                                                <div class="tabs-content">
                                                    <div class="series-table">
                                                        <ul>
                                                            <li>
                                                                <a href="#" class="series-table-image">
                                                                    <img src="images/custom_table_img.png" alt="" class="img-responsive">
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="series-table-name" href="#">Стол рабочий</a>
                                                            </li>
                                                            <li>
                                                                <div class="form-number">
                                                                    <span class="minus">-</span>
                                                                    <span class="plus">+</span>
                                                                    <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                                </div>
                                                            </li>
                                                            <li><span class="price-old">2 х 3 816 <i class="fa fa-ruble"></i></span> <span class="price-new">7 632 <i class="fa fa-ruble"></i></span> </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <a href="#" class="series-table-image">
                                                                    <img src="images/custom_table_img.png" alt="" class="img-responsive">
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="series-table-name" href="#">Стол рабочий</a>
                                                            </li>
                                                            <li>
                                                                <div class="form-number">
                                                                    <span class="minus">-</span>
                                                                    <span class="plus">+</span>
                                                                    <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                                </div>
                                                            </li>
                                                            <li <span class="price-new">7 632 <i class="fa fa-ruble"></i></span> </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <a href="#" class="series-table-image">
                                                                    <img src="images/custom_table_img.png" alt="" class="img-responsive">
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="series-table-name" href="#">Стол рабочий</a>
                                                            </li>
                                                            <li>
                                                                <div class="form-number">
                                                                    <span class="minus">-</span>
                                                                    <span class="plus">+</span>
                                                                    <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                                </div>
                                                            </li>
                                                            <li><span class="price-old">2 х 3 816 <i class="fa fa-ruble"></i></span> <span class="price-new">7 632 <i class="fa fa-ruble"></i></span> </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="series-colors">
                                                        <li>
                                                            <span>Цвет:</span>
                                                        </li>
                                                        <li>
                                                            <div class="colors">
                                                                <ul class="colors-list">
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>

                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio">
                                                                            <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>

                                                                    <li class="last">
                                                                        <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                                    </li>
                                                                </ul>

                                                                <ul class="colors-current">
                                                                    <li>
                                                                        <label>
                                                                            <input type="radio" name="radio" checked disabled>
                                                                            <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                        </label>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                                    </li>
                                                                    <li>
                                                                        <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <ul class="series-total">
                                                        <li>Итого</li>
                                                        <li>4 товара</li>
                                                        <li>18 728 <i class="fa fa-ruble"></i></li>
                                                    </ul>
                                                    <ul class="series-buy">
                                                        <li>
                                                            <a href="#" class="btn" type="submit">Быстрая покупка</a>
                                                        </li>
                                                        <li>
                                                            <button class="btn btn-orange" type="submit"><i class="fa fa-shopping-cart"></i> В корзину</button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="series-tabs-item tab2">
                                                <div class="tabs-header" data-target=".nav2">Описание</div>
                                                <div class="tabs-content">2</div>
                                            </div>

                                            <div class="series-tabs-item tab3">
                                                <div class="tabs-header" data-target=".nav3">Характеристики</div>
                                                <div class="tabs-content">3</div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="heading-gray">
                        <div class="container">
                            <div class="border-wrap">
                                <h3>Подбор серии по элементам</h3>
                            </div>
                        </div>
                    </div>

                    <div class="filter">
                        <div class="container">
                            <div class="border-wrap">
                                <ul class="filter-row">
                                    <li>
                                        <ul class="filter-col">
                                            <li>
                                                <span class="filter-label">Показать:</span>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="views" class="custom-radio" value="Столы переговорные">
                                                    <div class="custom-label">
                                                        <svg class="ico-svg" viewBox="0 0 31 32">
                                                            <use xlink:href="img/sprite-icons.svg#icon-filter-all"></use>
                                                        </svg>
                                                        <span>Все товары</span>
                                                    </div>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="views"  class="custom-radio" value="Шкафы">
                                                    <div class="custom-label">
                                                        <svg class="ico-svg" viewBox="0 0 24 31">
                                                            <use xlink:href="img/sprite-icons.svg#icon-filter-shkaf"></use>
                                                        </svg>
                                                        <span>Шкафы</span>
                                                    </div>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="views"  class="custom-radio" value="Тумбы">
                                                    <div class="custom-label">
                                                        <svg class="ico-svg" viewBox="0 0 27 31">
                                                            <use xlink:href="img/sprite-icons.svg#icon-filter-tumba"></use>
                                                        </svg>
                                                        <span>Тумбы</span>
                                                    </div>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="views"  class="custom-radio" value="Стеллажи">
                                                    <div class="custom-label">
                                                        <svg class="ico-svg" viewBox="0 0 21 31">
                                                            <use xlink:href="img/sprite-icons.svg#icon-filter-stellag"></use>
                                                        </svg>
                                                        <span>Стеллажи</span>
                                                    </div>
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input type="radio" name="views"  class="custom-radio" value="Столы переговорные">
                                                    <div class="custom-label">
                                                        <svg class="ico-svg" viewBox="0 0 35 31">
                                                            <use xlink:href="img/sprite-icons.svg#icon-filter-peregovornaja"></use>
                                                        </svg>
                                                        <span>Столы переговорные</span>
                                                    </div>
                                                </label>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="filter-nav">
                                            <div class="filter-nav-bg"></div>
                                            <div class="filter-nav-wrap">
                                                <a class="filter-nav-current" href="#">
                                                    <i class="fa fa-chevron-down"></i>
                                                    <span>Ещё</span>
                                                </a>

                                                <ul class="filter-nav-dropdown">
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views" class="custom-radio" value="Столы">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 35 33">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-stol"></use>
                                                                </svg>
                                                                <span>Столы</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views" class="custom-radio" value="мягкие Кресла">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 32 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-mjagkie-kresla"></use>
                                                                </svg>
                                                                <span>мягкие Кресла</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="стулья и кресла">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 20 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-stulja-kresla"></use>
                                                                </svg>
                                                                <span>стулья и кресла</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="Диваны">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 41 26">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-divany"></use>
                                                                </svg>
                                                                <span>Диваны</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="Шкафы">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 24 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-shkaf"></use>
                                                                </svg>
                                                                <span>Шкафы</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="Тумбы">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 27 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-tumba"></use>
                                                                </svg>
                                                                <span>Тумбы</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="Стеллажи">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 21 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-stellag"></use>
                                                                </svg>
                                                                <span>Стеллажи</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views"  class="custom-radio" value="Столы переговорные">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 35 31">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-peregovornaja"></use>
                                                                </svg>
                                                                <span>Столы переговорные</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                    <li>
                                                        <label>
                                                            <input type="radio" name="views" class="custom-radio" value="Столы переговорные">
                                                            <div class="custom-label">
                                                                <svg class="ico-svg" viewBox="0 0 31 32">
                                                                    <use xlink:href="img/sprite-icons.svg#icon-filter-all"></use>
                                                                </svg>
                                                                <span>Все товары</span>
                                                            </div>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="sort">
                        <div class="container">
                            <div class="border-wrap">
                                <div class="clearfix">

                                    <div class="sort-group-left">
                                        <span class="sort-legend">Сортировать:</span>
                                        <ul class="sort-elem">
                                            <li>По цене</li>
                                            <li>
                                                <label class="sort-item sort-item-down">
                                                    <input type="radio" class="sort-radio" name="price">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-down"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                                <label class="sort-item sort-item-up">
                                                    <input type="radio" class="sort-radio" name="price">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-up"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                        <ul class="sort-elem">
                                            <li>По популярности</li>
                                            <li>
                                                <label class="sort-item sort-item-down">
                                                    <input type="radio" class="sort-radio" name="pop">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-down"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                                <label class="sort-item sort-item-up">
                                                    <input type="radio" class="sort-radio" name="pop">
                                                    <span class="sort-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 7 7" name="pop">
                                                <use xlink:href="img/sprite-icons.svg#icon-sort-arrow-up"></use>
                                            </svg>
                                        </i>
                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="sort-group-right">
                                        <span class="sort-legend">Показывать только:</span>
                                        <ul class="sort-type">
                                            <li>
                                                <label class="type-label">
                                                    <input type="checkbox" class="checkbox" name="pop">
                                                    <span class="checkbox-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 13 20">
                                                <use xlink:href="img/sprite-icons.svg#icon-favorite"></use>
                                            </svg>
                                            Акции
                                        </i>
                                    </span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="type-label">
                                                    <input type="checkbox" class="checkbox" name="pop">
                                                    <span class="checkbox-text">
                                        <i>
                                            <svg class="ico-svg" viewBox="0 0 16 15">
                                                <use xlink:href="img/sprite-icons.svg#icon-new"></use>
                                            </svg>
                                            Новинки
                                        </i>
                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="content">

                        <div class="container">
                            <div class="border-wrap">

                                <ul class="product-tile">
                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <!-- Favorite -->
                                                <?php include('inc/favorite.inc.php') ?>
                                                <!-- -->

                                                <a href="#" class="showcase-image">
                                                    <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                            </li>
                                                        </ul>

                                                        <ul class="colors-current">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio" checked disabled>
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                            </li>
                                                            <li>
                                                                <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <!-- Favorite -->
                                                <?php include('inc/favorite.inc.php') ?>
                                                <!-- -->

                                                <a href="#" class="showcase-image">
                                                    <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors colors-selected">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                            </li>
                                                        </ul>

                                                        <ul class="colors-current">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio" checked disabled>
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                            </li>
                                                            <li>
                                                                <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <!-- Favorite -->
                                                <?php include('inc/favorite.inc.php') ?>
                                                <!-- -->

                                                <a href="#" class="showcase-image">
                                                    <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                            </li>
                                                        </ul>

                                                        <ul class="colors-current">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio" checked disabled>
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                            </li>
                                                            <li>
                                                                <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <!-- Favorite -->
                                                <?php include('inc/favorite.inc.php') ?>
                                                <!-- -->

                                                <a href="#" class="showcase-image">
                                                    <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors colors-selected">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                            </li>
                                                        </ul>

                                                        <ul class="colors-current">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio" checked disabled>
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                            </li>
                                                            <li>
                                                                <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <!-- Favorite -->
                                                <?php include('inc/favorite.inc.php') ?>
                                                <!-- -->

                                                <a href="#" class="showcase-image">
                                                    <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors colors-selected">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                            </li>
                                                        </ul>

                                                        <ul class="colors-current">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio" checked disabled>
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                            </li>
                                                            <li>
                                                                <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <!-- Favorite -->
                                                <?php include('inc/favorite.inc.php') ?>
                                                <!-- -->

                                                <a href="#" class="showcase-image">
                                                    <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors colors-selected">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                            </li>
                                                        </ul>

                                                        <ul class="colors-current">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio" checked disabled>
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                            </li>
                                                            <li>
                                                                <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <!-- Favorite -->
                                                <?php include('inc/favorite.inc.php') ?>
                                                <!-- -->

                                                <a href="#" class="showcase-image">
                                                    <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors colors-selected">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                            </li>
                                                        </ul>

                                                        <ul class="colors-current">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio" checked disabled>
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                            </li>
                                                            <li>
                                                                <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="showcase">
                                            <form class="form">

                                                <!-- Favorite -->
                                                <?php include('inc/favorite.inc.php') ?>
                                                <!-- -->

                                                <a href="#" class="showcase-image">
                                                    <img src="images/custom_table_img.png" class="img-responsive" alt="">
                                                </a>
                                                <div class="showcase-content">
                                                    <h4 class="showcase-name"><a href="#">Стул Cappuccino chrome</a></h4>
                                                    <div class="showcase-size">Размеры (Ш х Г х В): 280 х 360 х 480 мм</div>
                                                    <div class="colors colors-selected">
                                                        <ul class="colors-list">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_01.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_02.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_03.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_04.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio">
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li class="last">
                                                                <a href="#modal-color" class="btn-more-colors btn-modal-color">Еще цвета</a>
                                                            </li>
                                                        </ul>

                                                        <ul class="colors-current">
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="radio" checked disabled>
                                                                    <span><img src="images/colors/color_05.jpg" alt=""></span>
                                                                </label>
                                                            </li>
                                                            <li>
                                                                <a href="#modal-color" class="color-name btn-modal-color">Экокожа OREGON 12...</a>
                                                            </li>
                                                            <li>
                                                                <span class="colors-price">0 <i class="fa fa-ruble"></i></span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <ul class="showcase-purchase">
                                                        <li>
                                                            <div class="form-number">
                                                                <span class="minus">-</span>
                                                                <span class="plus">+</span>
                                                                <input type="text" class="form-number-control" name="form-number-control" value="1">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <span class="price-old">3 209 <i class="fa fa-ruble"></i></span>
                                                            <span class="price">3 009 <i class="fa fa-ruble"></i></span>
                                                        </li>
                                                    </ul>

                                                    <button type="submit" class="btn">В корзину</button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>
                    </div>

                </section>
                <!-- -->

                <!-- Leaders block -->
                <?php include('inc/leaders.inc.php') ?>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

        <script>

            $(function($){
                var point = $('.page-inner');
                var label = $('.filter');
                $h = label.offset().top;

                $(window).scroll(function(){

                    if ( $(window).scrollTop() > $h) {
                        point.addClass('fix-filter');
                    }else{
                        //Иначе возвращаем всё назад. Тут вы вносите свои данные
                        point.removeClass('fix-filter');
                    }
                });
            });
        </script>

    </body>
</html>
