<!doctype html>
<html class="no-js" lang="ru">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body>

        <div class="page">
            <div class="page-inner">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/navbar.inc.php') ?>
                <!-- -->

                <!-- Main page -->
                <section class="main">

                    <!-- Main heading -->
                    <div class="heading">
                        <div class="container">
                            <div class="border-wrap">

                                <!-- Breadcrumbs -->
                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li>Новости</li>
                                </ul>
                                <!-- -->

                                <!-- Heading -->
                                <h1>Новости</h1>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <!-- Nav heading -->
                    <nav class="heading-nav">
                        <div class="container">
                            <ul>
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">Наши клиенты</a></li>
                                <li class="active"><a href="#">Новости</a></li>
                                <li><a href="#">Галерея</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- -->

                    <!-- Main content -->
                    <div class="content">
                        <div class="container">

                            <div class="news-row">

                                <div class="news-row-item">
                                    <a class="news-item" href="#">
                                        <div class="news-image">
                                            <img src="images/news_01.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Моно-Люкс - цены снижены!</h4>
                                            <p>Мебель для персонала Моно-Люкс теперь еще выгоднее! Новые привлекательные цены в нашем интернет-магазине!</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="news-row-item">
                                    <a class="news-item" href="#" >
                                        <div class="news-image">
                                            <img src="images/news_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Мебель Альтернатива - в каталоге!</h4>
                                            <p>Наш каталог пополнен новой серией мебели Альтернатива! Серия участвует в проходящей акции по фабрике Экспро!</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="news-row-item">
                                    <a class="news-item" href="#">
                                        <div class="news-image">
                                            <img src="images/news_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Ресепшн Ринг доступен для заказа!</h4>
                                            <p>Ресепшн Ринг - доступный и стильный вариант ресепшн для современного офиса! Необычный дизайн угловым...</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="news-row-item">
                                    <a class="news-item" href="#" >
                                        <div class="news-image">
                                            <img src="images/news_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Мебель Альтернатива - в каталоге!</h4>
                                            <p>Наш каталог пополнен новой серией мебели Альтернатива! Серия участвует в проходящей акции по фабрике Экспро!</p>
                                        </div>
                                    </a>
                                </div>


                                <div class="news-row-item">
                                    <a class="news-item" href="#">
                                        <div class="news-image">
                                            <img src="images/news_01.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Моно-Люкс - цены снижены!</h4>
                                            <p>Мебель для персонала Моно-Люкс теперь еще выгоднее! Новые привлекательные цены в нашем интернет-магазине!</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="news-row-item">
                                    <a class="news-item" href="#" >
                                        <div class="news-image">
                                            <img src="images/news_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Мебель Альтернатива - в каталоге!</h4>
                                            <p>Наш каталог пополнен новой серией мебели Альтернатива! Серия участвует в проходящей акции по фабрике Экспро!</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="news-row-item">
                                    <a class="news-item" href="#">
                                        <div class="news-image">
                                            <img src="images/news_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Ресепшн Ринг доступен для заказа!</h4>
                                            <p>Ресепшн Ринг - доступный и стильный вариант ресепшн для современного офиса! Необычный дизайн угловым...</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="news-row-item">
                                    <a class="news-item" href="#" >
                                        <div class="news-image">
                                            <img src="images/news_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Мебель Альтернатива - в каталоге!</h4>
                                            <p>Наш каталог пополнен новой серией мебели Альтернатива! Серия участвует в проходящей акции по фабрике Экспро!</p>
                                        </div>
                                    </a>
                                </div>


                                <div class="news-row-item">
                                    <a class="news-item" href="#">
                                        <div class="news-image">
                                            <img src="images/news_01.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Моно-Люкс - цены снижены!</h4>
                                            <p>Мебель для персонала Моно-Люкс теперь еще выгоднее! Новые привлекательные цены в нашем интернет-магазине!</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="news-row-item">
                                    <a class="news-item" href="#" >
                                        <div class="news-image">
                                            <img src="images/news_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Мебель Альтернатива - в каталоге!</h4>
                                            <p>Наш каталог пополнен новой серией мебели Альтернатива! Серия участвует в проходящей акции по фабрике Экспро!</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="news-row-item">
                                    <a class="news-item" href="#">
                                        <div class="news-image">
                                            <img src="images/news_03.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Ресепшн Ринг доступен для заказа!</h4>
                                            <p>Ресепшн Ринг - доступный и стильный вариант ресепшн для современного офиса! Необычный дизайн угловым...</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="news-row-item">
                                    <a class="news-item" href="#" >
                                        <div class="news-image">
                                            <img src="images/news_02.jpg" class="img-responsive" alt="">
                                        </div>
                                        <div class="news-content">
                                            <h4>Новинка! Мебель Альтернатива - в каталоге!</h4>
                                            <p>Наш каталог пополнен новой серией мебели Альтернатива! Серия участвует в проходящей акции по фабрике Экспро!</p>
                                        </div>
                                    </a>
                                </div>


                            </div>
                        </div>
                    </div>
                    <!-- -->

                    <div class="pagination">
                        <div class="container">
                            <ul>
                                <li><a href="#">НАЗАД</a></li>
                                <li class="current"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">ВПЕРЕД</a></li>
                            </ul>
                        </div>
                    </div>

                </section>
                <!-- -->

                <!-- Leaders block -->
                <?php include('inc/leaders.inc.php') ?>
                <!-- -->

            </div>

            <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
            <!-- -->

        </div>

        <!-- Footer -->
        <?php include('inc/script.inc.php') ?>
        <!-- -->

    </body>
</html>
